﻿using ImovieDb.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using MovieDb.Data.Context;
using MovieDb.Data.Models;
using MovieDb.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace MovieDb.Services
{
    public class DirectorService : IDirectorService
    {
        private readonly MovieDbContext context;

        public DirectorService(MovieDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Director> AddDirectorAsync(string movieTitle, string firstName, string lastName)
        {
            var movie = await this.context.Movies
                .Include(m => m.Director)
                .FirstOrDefaultAsync(m => m.Title == movieTitle);

            if ( movie == null)
            {
                throw new MovieNotFoundException($"Movie with title \"{movieTitle}\" you are trying to add director to doesn't exist in the database!");
            }

            var director = movie.Director;

            if (director != null)
            {
                throw new DirectorAlreadyAssignedException($"There is already assigned director to the movie with title \"{movieTitle}\"!");
            }

            director = await this.context.Directors
                .FirstOrDefaultAsync(d => d.FirstName == firstName && d.LastName == lastName);

            if (director == null)
            {
                director = new Director() { FirstName = firstName, LastName = lastName };
                await this.context.Directors.AddAsync(director);
            }
            
            movie.Director = director;
            await this.context.SaveChangesAsync();
            return director;
        }
    }
}
