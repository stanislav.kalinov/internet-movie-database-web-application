﻿using ImovieDb.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using MovieDb.Data.Context;
using MovieDb.Data.Models;
using MovieDb.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace MovieDb.Services
{
    public class PreviewService : IPreviewService
    {
        private readonly MovieDbContext context;

        public PreviewService(MovieDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Preview> AddPreviewAsync(string movieTitle, string content)
        {
            var movie = await this.context.Movies.Include(m => m.Preview).FirstOrDefaultAsync(m => m.Title == movieTitle);

            if (movie == null)
            {
                throw new MovieNotFoundException($"Movie with title \"{movieTitle}\" you are trying to add preview to doesn't exist in the database!");
            }

            var preview = movie.Preview;

            if (preview != null)
            {
                throw new PreviewAlreadyAssignedException($"There is already assigned preview to the movie with title \"{movieTitle}\"!");
            }

            preview = new Preview() { Content = content };
            movie.Preview = preview;

            await this.context.Previews.AddAsync(preview);
            await this.context.SaveChangesAsync();

            return preview;
        }

        public async Task<Preview> ChangePreviewAsync(string movieTitle, string newContent)
        {
            var movie = await this.context.Movies.Include(m => m.Preview).FirstOrDefaultAsync(m => m.Title == movieTitle);

            if (movie == null)
            {
                throw new MovieNotFoundException($"Movie with title \"{movieTitle}\" you are trying to change preview doesn't exist in the database!");
            }

            var preview = movie.Preview;

            if (preview == null)
            {
                throw new PreviewNotFoundException($"There is no assigned preview to the movie with title \"{movieTitle}\"!");
            }

            this.context.Previews.Remove(preview);

            var newPreview = new Preview() { Content = newContent };

            movie.Preview = newPreview;

            await this.context.Previews.AddAsync(newPreview);
            await this.context.SaveChangesAsync();

            return newPreview;
        }
    }
}
