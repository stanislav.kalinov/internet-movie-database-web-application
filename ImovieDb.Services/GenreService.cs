﻿using ImovieDb.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using MovieDb.Data.Context;
using MovieDb.Data.Models;
using MovieDb.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDb.Services
{
    public class GenreService : IGenreService
    {
        private readonly MovieDbContext context;

        public GenreService(MovieDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Task<Genre> GetMainGenreAsync(string childGenre)
        {
            Task<Genre> mainGenre;

            switch (childGenre.ToLower())
            {
                case "comedy":
                     mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Feature film");
                    break;
                case "drama":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Feature film");
                    break;
                case "action":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Feature film");
                    break;
                case "thriller":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Feature film");
                    break;
                case "horror":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Feature film");
                    break;
                case "western":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Feature film");
                    break;
                case "fantasy":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Feature film");
                    break;
                case "crime":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Feature film");
                    break;
                case "historical":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Documentary");
                    break;
                case "investigasion":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Documentary");
                    break;
                case "medicine":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Documentary");
                    break;
                case "space":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Documentary");
                    break;
                case "technologies":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Documentary");
                    break;
                case "natural sciences":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Documentary");
                    break;
                case "science fiction":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Documentary");
                    break;
                case "traditional animation":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Animation");
                    break;
                case "stop motion":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Animation");
                    break;
                case "computer animation":
                    mainGenre = this.context.Genres.FirstOrDefaultAsync(g => g.Type == "Animation");
                    break;
                default:
                    throw new MovieDbArgumentException("Please provide valid genre type!");
            }

            return mainGenre;
        }

        public async Task<Genre> AddGenreAsync(string movieTitle, string genreType)
        {
            var movie = await this.context.Movies.Include(m => m.MainGenre).Include(m => m.MoviesGenres).ThenInclude(mg => mg.Genre).FirstOrDefaultAsync(m => m.Title == movieTitle);

            if (movie == null)
            {
                throw new MovieNotFoundException($"Movie with title \"{movieTitle}\" you are trying to add genre to doesn't exist in the database!");
            }

            var genre = movie.MoviesGenres.FirstOrDefault(mg => mg.Genre.Type == genreType)?.Genre;

            if (genre != null)
            {
                throw new GenreAlreadyAssignedException($"There is already assigned genre with type \"{genreType}\" to the movie with title \"{movieTitle}\"!");
            }

            var mainGenre = await this.GetMainGenreAsync(genreType);

            if (mainGenre == null)
            {
                throw new MainGenreNotFoundException("Please add corresponding main genre to the database!");
            }

            if (movie.MainGenre != null && movie.MainGenre != mainGenre)
            {
                throw new MainGenreNotCorrespondingException("You are trying to add genre that is not in the respective main genre list!");
            }

            genre = await this.context.Genres.FirstOrDefaultAsync(g => g.Type == genreType);

            if (genre == null)
            {
                genre = new Genre() { Type = genreType };
                await this.context.Genres.AddAsync(genre);
                genre.MainGenre = mainGenre;
            }

            await this.context.MoviesGenres.AddAsync(new MoviesGenres() { Movie = movie, Genre = genre });

            if (movie.MainGenre == null)
            {
                movie.MainGenre = mainGenre;
            }
            
            await this.context.SaveChangesAsync();

            return genre;
        }

        public async Task DeleteGenreAsync(string movieTitle, string genreType)
        {
            var movie = await this.context.Movies.Include(m => m.MainGenre).Include(m => m.MoviesGenres).ThenInclude(mg => mg.Genre).FirstOrDefaultAsync(m => m.Title == movieTitle);

            if (movie == null)
            {
                throw new MovieNotFoundException($"Movie with title \"{movieTitle}\" you are trying to delete genre from doesn't exist in the database!");
            }

            var moviesGenres = movie.MoviesGenres;
            var movieGenre = movie.MoviesGenres.FirstOrDefault(mg => mg.Genre.Type == genreType);

            if (movieGenre == null)
            {
                throw new GenreNotFoundException($"There is not genre with type \"{genreType}\" assigned to the movie with title \"{movieTitle}\"!");
            }

            if (moviesGenres.Count == 1)
            {
                movie.MainGenre = null;
            }

            this.context.MoviesGenres.Remove(movieGenre);
            await this.context.SaveChangesAsync();
        }

        public async Task<Genre> AddMainGenreAsync(string genreType)
        {
            if (genreType != "Feature film" && genreType != "Documentary" && genreType != "Animation")
            {
                throw new MovieDbArgumentException("Please provide valid main genre type: [Feature film], [Documentary] or [Animation]");
            }

            var genre = await this.context.Genres.FirstOrDefaultAsync(g => g.Type == genreType);

            if (genre != null)
            {
                throw new MainGenreAlreadyExistsException($"There is already added main genre with type \"{genreType}\" to the database!");
            }

            genre = new Genre() { Type = genreType };
            await this.context.Genres.AddAsync(genre);

            await this.context.SaveChangesAsync();

            return genre;
        }

        public async Task<IReadOnlyCollection<Genre>> GetChildGenresAsync()
        {
            var childGenres = this.context.Genres.Where(g => g.Type != "Feature film" && g.Type != "Documentary" && g.Type != "Animation").Distinct();
                
            return await childGenres.ToListAsync();
        }
    }
}
