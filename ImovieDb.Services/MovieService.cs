﻿//using iTextSharp.text;
//using iTextSharp.text.pdf;
using ImovieDb.Services.Exceptions;
using IMovieDb.Data.Models;
using Microsoft.EntityFrameworkCore;
using MovieDb.Data.Context;
using MovieDb.Data.Models;
using MovieDb.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace MovieDb.Services
{
    public class MovieService : IMovieService
    {
        private readonly MovieDbContext context;

        public MovieService(MovieDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        //public string PrintMovie(Movie movie)
        //{
        //    StringBuilder builder = new StringBuilder();

        //    builder.AppendLine($"{movie.Title} ({movie.ReleaseDate.ToString("MMMM yyyy")})");
        //    builder.AppendLine($"{movie.Duration} min - {movie.MainGenre?.Type} | {string.Join(" | ", movie.MoviesGenres.Select(mg => mg.Genre.Type))}");
        //    builder.AppendLine($"Rating: {movie.Rating}");
        //    builder.AppendLine($"{movie.Preview?.Content}");
        //    builder.AppendLine($"Director: {movie.Director?.FirstName} {movie.Director?.LastName}");
        //    builder.AppendLine($"Stars: {string.Join(", ", movie.MoviesActors.Select(ma => string.Format($"{ma.Actor.FirstName} {ma.Actor.LastName}")))}");
        //    builder.AppendLine();

        //    return builder.ToString();
        //}

        public async Task<Movie> AddMovieAsync(string title, DateTime releaseDate, int duration)
        {
            if (await this.context.Movies.AnyAsync(m => m.Title == title))
            {
                throw new MovieAlreadyExistsException($"The movie with title \"{title}\" has been already added to the database!");
            }

            Movie movie = new Movie()
            {
                Title = title,
                ReleaseDate = releaseDate,
                Duration = duration
            };

            await this.context.Movies.AddAsync(movie);
            await this.context.SaveChangesAsync();

            return movie;
        }

        public async Task<Movie> AddRatingAsync(string movieTitle, int rating)
        {
            if (rating < 1 || rating > 10)
            {
                throw new MovieDbArgumentException($"The movie rating must be an integer in the range [1-10]!");
            }

            var movie = await this.context.Movies.FirstOrDefaultAsync(m => m.Title == movieTitle);

            if (movie == null)
            {
                throw new MovieNotFoundException($"Movie with title \"{movieTitle}\" you are trying to add rating to doesn't exist in the database!");
            }

            if (movie.Rating != null)
            {
                throw new RatingAlreadyExistsException($"The rating to movie with title \"{movieTitle}\" you are trying to add rating to has been already added!");
            }

            movie.Rating = rating;

            await this.context.SaveChangesAsync();

            return movie;
        }

        public async Task<(Movie, int)> ChangeRatingAsync(string movieTitle, int newRating)
        {
            if (newRating < 1 || newRating > 10)
            {
                throw new MovieDbArgumentException($"The movie rating must be an integer in the range [1-10]!");
            }

            var movie = await this.context.Movies.FirstOrDefaultAsync(m => m.Title == movieTitle);

            if (movie == null)
            {
                throw new MovieNotFoundException($"Movie with title \"{movieTitle}\" you are trying to change rating doesn't exist in the database!");
            }

            if (movie.Rating == null)
            {
                throw new RatingNotFoundException($"Movie with title \"{movieTitle}\" hasn't got rating added!");
            }

            int oldRating = (int)movie.Rating;

            if (oldRating == newRating)
            {
                throw new RatingAlreadyExistsException($"The rating \"{newRating}\" is already assigned to movie with title \"{movieTitle}\" !");
            }

            movie.Rating = newRating;

            await this.context.SaveChangesAsync();

            return (movie, oldRating);
        }
        public async Task<Review> AddReviewAsync(int movieId, string content)
        {
            var movie = await this.context.Movies.Include(m => m.Reviews).FirstOrDefaultAsync(m => m.Id == movieId);

            var review = new Review() { Content = content };
            movie.Reviews.Add(review);

            await this.context.Reviews.AddAsync(review);
            await this.context.SaveChangesAsync();

            return review;
        }

        public async Task DeleteMovieAsync(string movieTitle)
        {
            var movie = await this.context.Movies
                .Include(m => m.MoviesActors)
                .Include(m => m.MoviesGenres)
                .Include(m => m.Preview)
                .FirstOrDefaultAsync(m => m.Title == movieTitle);

            if (movie == null)
            {
                throw new MovieNotFoundException($"Movie with title \"{movieTitle}\" you are trying to delete doesn't exist in the database!");
            }

            foreach (var movieActor in movie.MoviesActors)
            {
                this.context.MoviesActors.Remove(movieActor);
            }

            foreach (var movieGenre in movie.MoviesGenres)
            {
                this.context.MoviesGenres.Remove(movieGenre);
            }

            if (movie.Preview != null)
            {
                this.context.Previews.Remove(movie.Preview);
            }
            
            this.context.Movies.Remove(movie);
            await this.context.SaveChangesAsync();
        }

        public async Task<Movie> GetMovieToShowAsync(int Id)
        {
            var movie = await this.context.Movies.Include(m => m.MainGenre)
                .Include(m => m.MoviesActors).ThenInclude(ma => ma.Actor)
                .Include(m => m.MoviesGenres).ThenInclude(mg => mg.Genre)
                .Include(m => m.Preview).Include(m => m.Director)
                .Include(m=>m.Reviews)
                .FirstOrDefaultAsync(m => m.Id == Id);

            if (movie == null)
            {
                throw new MovieNotFoundException($"Movie you are trying to show summary doesn't exist in the database!");
            }

            return movie;
        }

        public async Task<IReadOnlyCollection<Movie>> GetTopRatedMovies()
        {
            return await this.context.Movies.Where(m => m.Rating >= 8).OrderByDescending(m => m.Rating).ToListAsync();
        }

        public async Task<IReadOnlyCollection<Movie>> GetMoviesByGenre(IReadOnlyCollection<string> genreType)
        {
            List<Genre> genre = new List<Genre>();
            foreach (var item in genreType)
            {
               var genres = await this.context.Genres.FirstOrDefaultAsync(g => g.Type == item);
                genre.Add(genres);
            }
            if (!genre.Any())
            {
                throw new GenreNotFoundException($"Genre with type {genreType} doesn't exist in the database!");
            }
            List<Movie> movie = new List<Movie>();
            foreach(var item in genre)
            {
                var movies = await this.context.MoviesGenres.Where(mg => mg.Genre == item).Select(mg => mg.Movie).Include(m => m.MainGenre)
                             .Include(m => m.MoviesActors).ThenInclude(ma => ma.Actor).Include(m => m.MoviesGenres).ThenInclude(mg => mg.Genre)
                             .Include(m => m.Preview).Include(m => m.Director).ToListAsync();
                foreach (var iten in movies)
                {
                    if (movie.Contains(iten))
                    {
                        continue;
                    }
                    movie.Add(iten);
                }
            }
            return movie;
        }
        public async Task<Movie> AddUserRatingAsync(int movieId, int rating)
        {
            if (rating < 1 || rating > 10)
            {
                throw new MovieDbArgumentException($"The movie rating must be an integer in the range [1-10]!");
            }

            var movie = await this.context.Movies.FirstOrDefaultAsync(m => m.Id == movieId);
            movie.UserRating = (movie.UserRating ?? 0) + rating;
            movie.Voters = (movie.Voters ?? 0)+ 1;
            movie.UserRating = movie.UserRating / movie.Voters;
            await this.context.SaveChangesAsync();


            return (movie);
        }

        public async Task<IReadOnlyCollection<Movie>> GetMoviesByMainGenreAsync(string genreType)
        {
            var mainGenre = await this.context.Genres.FirstOrDefaultAsync(g => g.Type == genreType);

            if (mainGenre == null)
            {
                throw new MainGenreNotFoundException($"Main genre with type \"{genreType}\" doesn't exist in the database!");
            }

            var movies = this.context.Movies.Where(m => m.MainGenre == mainGenre).Include(m => m.MainGenre)
                 .Include(m => m.MoviesActors).ThenInclude(ma => ma.Actor).Include(m => m.MoviesGenres).ThenInclude(mg => mg.Genre)
                 .Include(m => m.Preview).Include(m => m.Director);


            return await movies.ToListAsync();
        }
        public async Task <IReadOnlyCollection<Movie>> GetMoviesByActorAsync(string firstName, string lastName)
        {
            var actor = await this.context.Actors.FirstOrDefaultAsync(a => a.FirstName == firstName && a.LastName == lastName);
            if (actor == null)
            {
                throw new ActorNotFoundException($"Actor with name \"{firstName} {lastName}\" doesn't exist in the database!");
            }

            var movies = this.context.MoviesActors.Where(ma => ma.Actor == actor).Select(ma => ma.Movie).Include(m => m.MainGenre)
                             .Include(m => m.MoviesActors).ThenInclude(ma => ma.Actor).Include(m => m.MoviesGenres).ThenInclude(mg => mg.Genre)
                             .Include(m => m.Preview).Include(m => m.Director)
                             .Include(m => m.Reviews); 

            return await movies.ToListAsync();
        }

        public async Task<IReadOnlyCollection<Movie>> GetMoviesByDirectorAsync(string firstName, string lastName)
        {
            var director = await this.context.Directors.FirstOrDefaultAsync(d => d.FirstName == firstName && d.LastName == lastName);

            if (director == null)
            {
                throw new DirectorNotFoundException($"Director with name \"{firstName} {lastName}\" doesn't exist in the database!");
            }

            var movies = this.context.Movies.Where(m => m.Director == director).Include(m => m.MainGenre)
                             .Include(m => m.MoviesActors).ThenInclude(ma => ma.Actor).Include(m => m.MoviesGenres).ThenInclude(mg => mg.Genre)
                             .Include(m => m.Preview).Include(m => m.Director)
                             .Include(m => m.Reviews); 

            return await movies.ToListAsync();
        }
        public async Task<IReadOnlyCollection<Movie>> GetCommingSoonMovies()
        {
            var movies = this.context.Movies.Where(m => m.ReleaseDate > DateTime.Now && m.ReleaseDate <= DateTime.Now.AddMonths(3)).Include(m => m.MainGenre)
                             .Include(m => m.MoviesActors).ThenInclude(ma => ma.Actor).Include(m => m.MoviesGenres).ThenInclude(mg => mg.Genre)
                             .Include(m => m.Preview).Include(m => m.Director)
                             .Include(m => m.Reviews); 

            return await movies.ToListAsync();
        }
        public IQueryable<Movie> AllMoviesQuery()
        {
            return this.context.Movies.Include(m => m.MainGenre)
                             .Include(m => m.MoviesActors).ThenInclude(ma => ma.Actor).Include(m => m.MoviesGenres).ThenInclude(mg => mg.Genre)
                             .Include(m => m.Preview).Include(m => m.Director)
                             .Include(m => m.Reviews);
        }

        public async Task<IReadOnlyCollection<Movie>> GetMoviesByReleaseYearAsync(int releaseYear)
        {
            var movies = this.context.Movies.Where(m => m.ReleaseDate.Year == releaseYear)
                             .Include(m => m.MainGenre)
                             .Include(m => m.MoviesActors).ThenInclude(ma => ma.Actor)
                             .Include(m => m.MoviesGenres).ThenInclude(mg => mg.Genre)
                             .Include(m => m.Preview).Include(m => m.Director)
                             .Include(m => m.Reviews); 

            return await movies.ToListAsync();
        }

        public async Task<IReadOnlyCollection<Movie>> GetMoviesByReleaseDateAsync(DateTime startDate, DateTime endDate)
        {
            var movies = this.context.Movies.Where(m => m.ReleaseDate >= startDate && m.ReleaseDate <= endDate || m.ReleaseDate <= startDate && m.ReleaseDate >= endDate)
                .Include(m => m.MainGenre)
                .Include(m => m.MoviesActors).ThenInclude(ma => ma.Actor)
                .Include(m => m.MoviesGenres).ThenInclude(mg => mg.Genre)
                .Include(m => m.Preview).Include(m => m.Director)
                .Include(m => m.Reviews); ;

            return await movies.ToListAsync();
        }

        public async Task<IReadOnlyCollection<Movie>> FilterMoviesAsync(string searchString, string movieGenre, string mainGenre)
        {
            var movies = this.context.Movies.AsQueryable();

            if (!string.IsNullOrEmpty(searchString))
            {
                movies = movies.Where(m => m.Title.Contains(searchString));
            }

            if (!string.IsNullOrEmpty(movieGenre))
            {
                movies = movies.Where(m => m.MoviesGenres.Any(mg => mg.Genre.Type == movieGenre));
            }

            if (!string.IsNullOrEmpty(mainGenre))
            {
                movies = movies.Where(m => m.MainGenre.Type == mainGenre);
            }


            return await movies.ToListAsync();
        }
    }
}


        //public void CreateMoviePDFReport(string output)
        //{
        //     FileStream fs = new FileStream("document.pdf", FileMode.Create);

        //    Document document = new Document(PageSize.A4, 25, 25, 30, 30);
        //    PdfWriter writer = PdfWriter.GetInstance(document, fs);

        //    document.Open();

        //    document.Add(new Paragraph(output));

        //    document.Close();
        //    writer.Close();
        //    fs.Close();

        //    System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo("document.pdf") { UseShellExecute = true });
        //}
