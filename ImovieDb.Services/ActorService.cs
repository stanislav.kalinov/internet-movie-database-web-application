﻿using ImovieDb.Services.Exceptions;
using Microsoft.EntityFrameworkCore;
using MovieDb.Data.Context;
using MovieDb.Data.Models;
using MovieDb.Services.Contracts;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDb.Services
{
    public class ActorService : IActorService
    {
        private readonly MovieDbContext context;

        public ActorService(MovieDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Actor> AddActorAsync(string movieTitle, string firstName, string lastName, DateTime? birthday)
        {
            var movie = await this.context.Movies.Include(m => m.MoviesActors).ThenInclude(ma => ma.Actor).FirstOrDefaultAsync(m => m.Title == movieTitle);

            if (movie == null)
            {
                throw new MovieNotFoundException($"Movie with title \"{movieTitle}\" you are trying to add actor to doesn't exist in the database!");
            }

            var actor = movie.MoviesActors.FirstOrDefault(ma => ma.Actor.FirstName == firstName && ma.Actor.LastName == lastName)?.Actor;

            if (actor != null)
            {
                throw new ActorAlreadyAssignedException($"There is already assigned actor with name \"{firstName} {lastName}\" to the movie with title \"{movieTitle}\"!");
            }

            actor = await this.context.Actors.FirstOrDefaultAsync(a => a.FirstName == firstName && a.LastName == lastName);

            if (actor == null)
            {
                actor = new Actor() { FirstName = firstName, LastName = lastName, Birthday = birthday };
                await this.context.Actors.AddAsync(actor);
            }

            await this.context.MoviesActors.AddAsync(new MoviesActors() { Movie = movie, Actor = actor });
            
            await this.context.SaveChangesAsync();

            return actor;
        }
    }
}
