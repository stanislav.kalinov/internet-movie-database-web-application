﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class MovieNotFoundException : BaseIMovieDbException
    {
        public MovieNotFoundException(string message) : base(message, (int)HttpStatusCode.NotFound)
        {
        }
    }
}
