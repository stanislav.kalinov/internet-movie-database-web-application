﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class ActorNotFoundException : BaseIMovieDbException
    {
        public ActorNotFoundException(string message) : base(message, (int)HttpStatusCode.NotFound)
        {
        }
    }
}
