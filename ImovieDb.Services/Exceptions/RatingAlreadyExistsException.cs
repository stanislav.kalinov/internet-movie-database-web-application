﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class RatingAlreadyExistsException : BaseIMovieDbException
    {
        public RatingAlreadyExistsException(string message) : base(message, (int)HttpStatusCode.Created)
        {
        }
    }
}
