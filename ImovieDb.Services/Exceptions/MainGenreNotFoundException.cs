﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class MainGenreNotFoundException : BaseIMovieDbException
    {
        public MainGenreNotFoundException(string message) : base(message, (int)HttpStatusCode.NotFound)
        {
        }
    }
}
