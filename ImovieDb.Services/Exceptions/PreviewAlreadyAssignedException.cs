﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class PreviewAlreadyAssignedException : BaseIMovieDbException
    {
        public PreviewAlreadyAssignedException(string message) : base(message, (int)HttpStatusCode.Created)
        {
        }
    }
}
