﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class DirectorAlreadyAssignedException : BaseIMovieDbException
    {
        public DirectorAlreadyAssignedException(string message) : base(message, (int)HttpStatusCode.Created)
        {
        }
    }
}
