﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class MainGenreNotCorrespondingException : BaseIMovieDbException
    {
        public MainGenreNotCorrespondingException(string message) : base(message, (int)HttpStatusCode.NotAcceptable)
        {
        }
    }
}
