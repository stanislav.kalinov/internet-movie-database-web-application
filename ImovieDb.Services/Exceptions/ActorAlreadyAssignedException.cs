﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class ActorAlreadyAssignedException : BaseIMovieDbException
    {
        public ActorAlreadyAssignedException(string message) : base(message, (int)HttpStatusCode.Created)
        {
        }
    }
}
