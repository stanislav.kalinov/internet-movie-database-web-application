﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class PreviewNotFoundException : BaseIMovieDbException
    {
        public PreviewNotFoundException(string message) : base(message, (int)HttpStatusCode.NotFound)
        {
        }
    }
}
