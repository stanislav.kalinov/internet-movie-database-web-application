﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class GenreAlreadyAssignedException : BaseIMovieDbException
    {
        public GenreAlreadyAssignedException(string message) : base(message, (int)HttpStatusCode.Created)
        {
        }
    }
}
