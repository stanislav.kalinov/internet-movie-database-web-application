﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class MainGenreAlreadyExistsException : BaseIMovieDbException
    {
        public MainGenreAlreadyExistsException(string message) : base(message, (int)HttpStatusCode.Created)
        {
        }
    }
}
