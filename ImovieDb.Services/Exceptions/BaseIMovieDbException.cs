﻿using System;

namespace ImovieDb.Services.Exceptions
{
    public class BaseIMovieDbException : Exception
    {
        private int code;

        public int Code { get; }

        public BaseIMovieDbException(string message, int code) : base(message)
        {
            this.code = code;
        }
    }
}
