﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class MovieDbArgumentException : BaseIMovieDbException
    {
        public MovieDbArgumentException(string message) : base(message, (int)HttpStatusCode.NotAcceptable)
        {
        }
    }
}
