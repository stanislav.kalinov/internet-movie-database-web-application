﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class GenreNotFoundException : BaseIMovieDbException
    {
        public GenreNotFoundException(string message) : base(message, (int)HttpStatusCode.NotFound)
        {
        }
    }
}
