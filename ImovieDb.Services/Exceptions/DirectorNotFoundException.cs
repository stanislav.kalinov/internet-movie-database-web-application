﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class DirectorNotFoundException : BaseIMovieDbException
    {
        public DirectorNotFoundException(string message) : base(message, (int)HttpStatusCode.NotFound)
        {
        }
    }
}
