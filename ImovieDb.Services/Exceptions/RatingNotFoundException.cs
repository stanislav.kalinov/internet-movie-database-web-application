﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class RatingNotFoundException : BaseIMovieDbException
    {
        public RatingNotFoundException(string message) : base(message, (int)HttpStatusCode.NotFound)
        {
        }
    }
}
