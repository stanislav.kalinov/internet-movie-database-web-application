﻿using System.Net;

namespace ImovieDb.Services.Exceptions
{
    public class MovieAlreadyExistsException : BaseIMovieDbException
    {
        public MovieAlreadyExistsException(string message) : base(message, (int)HttpStatusCode.Created)
        {
        }
    }
}
