﻿using MovieDb.Data.Models;
using System;
using System.Threading.Tasks;

namespace MovieDb.Services.Contracts
{
    public interface IActorService
    {
        Task<Actor> AddActorAsync(string movieTitle, string firstName, string lastName, DateTime? birthday);
    }
}
