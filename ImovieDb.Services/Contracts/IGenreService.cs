﻿using MovieDb.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieDb.Services.Contracts
{
    public interface IGenreService
    {
        Task<Genre> AddGenreAsync(string movieTitle, string genreType);
        Task DeleteGenreAsync(string movieTitle, string genreType);
        Task<Genre> AddMainGenreAsync(string genreType);
        Task<IReadOnlyCollection<Genre>> GetChildGenresAsync();
    }
}
