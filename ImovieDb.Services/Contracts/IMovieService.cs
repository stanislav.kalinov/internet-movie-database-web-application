﻿using IMovieDb.Data.Models;
using MovieDb.Data.Models;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieDb.Services.Contracts
{
    public interface IMovieService
    {
        Task<Movie> AddMovieAsync(string title, DateTime releaseDate, int duration);
        Task<Movie> AddRatingAsync(string movieTitle, int rating);
        Task<(Movie, int)> ChangeRatingAsync(string movieTitle, int newRating);
        Task DeleteMovieAsync(string movieTitle);
        Task<Movie> GetMovieToShowAsync(int Id);
        Task<IReadOnlyCollection<Movie>> GetTopRatedMovies();
        //string PrintMovie(Movie movie);
        Task<IReadOnlyCollection<Movie>> GetMoviesByActorAsync(string firstName, string lastName);
        Task<IReadOnlyCollection<Movie>> GetMoviesByDirectorAsync(string firstName, string lastName);
        Task<IReadOnlyCollection<Movie>> GetCommingSoonMovies();
        Task<IReadOnlyCollection<Movie>> GetMoviesByReleaseYearAsync(int releaseYear);
        Task<IReadOnlyCollection<Movie>> GetMoviesByReleaseDateAsync(DateTime startDate, DateTime endDate);
        Task<IReadOnlyCollection<Movie>> GetMoviesByGenre(IReadOnlyCollection<string> genreType);
        Task<IReadOnlyCollection<Movie>> GetMoviesByMainGenreAsync(string genreType);
        IQueryable<Movie> AllMoviesQuery();
        Task<IReadOnlyCollection<Movie>> FilterMoviesAsync(string searchString, string movieGenre, string mainGenre);
        Task<Review> AddReviewAsync(int movieId, string content);
        Task<Movie> AddUserRatingAsync(int movieId, int rating);
    }
}
