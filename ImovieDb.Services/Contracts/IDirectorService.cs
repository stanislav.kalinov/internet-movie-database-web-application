﻿using MovieDb.Data.Models;
using System.Threading.Tasks;

namespace MovieDb.Services.Contracts
{
    public interface IDirectorService
    {
        Task<Director> AddDirectorAsync(string movieTitle, string firstName, string lastName);
    }
}
