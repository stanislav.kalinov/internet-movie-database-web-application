﻿using MovieDb.Data.Models;
using System.Threading.Tasks;

namespace MovieDb.Services.Contracts
{
    public interface IPreviewService
    {
        Task<Preview> AddPreviewAsync(string movieTitle, string content);
        Task<Preview> ChangePreviewAsync(string movieTitle, string newContent);
    }
}
