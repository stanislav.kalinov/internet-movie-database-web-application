﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IMovieDb.Data.Migrations
{
    public partial class PisnaMI : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 771, DateTimeKind.Local).AddTicks(4435));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 777, DateTimeKind.Local).AddTicks(6990));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 777, DateTimeKind.Local).AddTicks(7227));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 777, DateTimeKind.Local).AddTicks(7533));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 777, DateTimeKind.Local).AddTicks(7623));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 777, DateTimeKind.Local).AddTicks(7730));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 777, DateTimeKind.Local).AddTicks(7800));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 777, DateTimeKind.Local).AddTicks(7880));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 9,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 777, DateTimeKind.Local).AddTicks(7951));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 10,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 777, DateTimeKind.Local).AddTicks(8029));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 11,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 777, DateTimeKind.Local).AddTicks(8328));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 792, DateTimeKind.Local).AddTicks(4363));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 794, DateTimeKind.Local).AddTicks(5845));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 794, DateTimeKind.Local).AddTicks(6085));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 794, DateTimeKind.Local).AddTicks(6216));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 28, 9, 794, DateTimeKind.Local).AddTicks(6306));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 324, DateTimeKind.Local).AddTicks(2317));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(5212));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(5448));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(5542));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(5609));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(5708));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(5776));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(5854));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 9,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(6065));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 10,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(6159));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 11,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(6243));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 345, DateTimeKind.Local).AddTicks(1213));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 347, DateTimeKind.Local).AddTicks(2236));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 347, DateTimeKind.Local).AddTicks(2471));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 347, DateTimeKind.Local).AddTicks(2608));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 21, 23, 57, 347, DateTimeKind.Local).AddTicks(2695));
        }
    }
}
