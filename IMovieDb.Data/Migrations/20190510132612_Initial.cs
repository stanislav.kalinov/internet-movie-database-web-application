﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IMovieDb.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Actors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(maxLength: 15, nullable: false),
                    LastName = table.Column<string>(maxLength: 15, nullable: false),
                    Birthday = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Directors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(maxLength: 15, nullable: false),
                    LastName = table.Column<string>(maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Directors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Genres",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Type = table.Column<string>(maxLength: 25, nullable: false),
                    MainGenreId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genres", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Genres_Genres_MainGenreId",
                        column: x => x.MainGenreId,
                        principalTable: "Genres",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Previews",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Content = table.Column<string>(type: "ntext", nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    MovieId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Previews", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Title = table.Column<string>(maxLength: 30, nullable: false),
                    ReleaseDate = table.Column<DateTime>(nullable: false),
                    Duration = table.Column<int>(nullable: false),
                    ImageURL = table.Column<string>(nullable: true),
                    Rating = table.Column<int>(nullable: true),
                    UserRating = table.Column<decimal>(nullable: true),
                    Voters = table.Column<int>(nullable: true),
                    DirectorId = table.Column<int>(nullable: true),
                    PreviewId = table.Column<int>(nullable: true),
                    MainGenreId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Directors_DirectorId",
                        column: x => x.DirectorId,
                        principalTable: "Directors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Movies_Genres_MainGenreId",
                        column: x => x.MainGenreId,
                        principalTable: "Genres",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_Movies_Previews_PreviewId",
                        column: x => x.PreviewId,
                        principalTable: "Previews",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "MoviesActors",
                columns: table => new
                {
                    MovieId = table.Column<int>(nullable: false),
                    ActorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MoviesActors", x => new { x.MovieId, x.ActorId });
                    table.ForeignKey(
                        name: "FK_MoviesActors_Actors_ActorId",
                        column: x => x.ActorId,
                        principalTable: "Actors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MoviesActors_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MoviesGenres",
                columns: table => new
                {
                    MovieId = table.Column<int>(nullable: false),
                    GenreId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MoviesGenres", x => new { x.MovieId, x.GenreId });
                    table.ForeignKey(
                        name: "FK_MoviesGenres_Genres_GenreId",
                        column: x => x.GenreId,
                        principalTable: "Genres",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MoviesGenres_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Reviews",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Content = table.Column<string>(type: "ntext", nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    MovieId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reviews", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reviews_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "Birthday", "FirstName", "LastName" },
                values: new object[,]
                {
                    { 14, new DateTime(1977, 1, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), "Orlando", "Bloom" },
                    { 18, new DateTime(1930, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Clint", "Eastwood" },
                    { 17, new DateTime(1977, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sarah", "Ballantyne" },
                    { 16, new DateTime(1972, 8, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "Cameron", "Diaz" },
                    { 15, new DateTime(1961, 4, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Eddie", "Murphy" },
                    { 13, new DateTime(1981, 1, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Elijah", "Wood" },
                    { 12, new DateTime(1969, 11, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Matthew", "McConaughy" },
                    { 11, new DateTime(1964, 8, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Georgi", "Staikov" },
                    { 10, new DateTime(1964, 1, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Hristo", "Shopov" },
                    { 9, new DateTime(1990, 7, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Margot", "Robbie" },
                    { 7, new DateTime(1980, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "Michelle", "Williams" },
                    { 8, new DateTime(1983, 12, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jonah", "Hill" },
                    { 5, new DateTime(1978, 8, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Matt", "Damon" },
                    { 4, new DateTime(1977, 6, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Christian", "Bale" },
                    { 3, new DateTime(1979, 4, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jessica", "Chastain" },
                    { 2, new DateTime(1971, 11, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Leonardo", "DiCaprio" },
                    { 1, new DateTime(1967, 6, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Brad", "Pitt" },
                    { 6, new DateTime(1977, 9, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tom", "Hardy" }
                });

            migrationBuilder.InsertData(
                table: "Directors",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[,]
                {
                    { 5, "Lenore", "Eklund" },
                    { 6, "Ruben", "Fleischer" },
                    { 7, "Martin", "Scorsese" },
                    { 8, "Ivan", "Andonov" },
                    { 9, "Christopher", "Nolan" },
                    { 10, "Peter", "Jackson" },
                    { 4, "Andrew", "Adamson" },
                    { 1, "Clint", "Eastwood" },
                    { 2, "Quentin", "Tarantino" },
                    { 3, "Steven", "Soderbergh" }
                });

            migrationBuilder.InsertData(
                table: "Genres",
                columns: new[] { "Id", "MainGenreId", "Type" },
                values: new object[,]
                {
                    { 1, null, "Feature film" },
                    { 2, null, "Documentary" },
                    { 3, null, "Animation" }
                });

            migrationBuilder.InsertData(
                table: "Previews",
                columns: new[] { "Id", "Content", "CreatedOn", "MovieId" },
                values: new object[,]
                {
                    { 5, "A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.", new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(7929), null },
                    { 4, "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.", new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(7826), null },
                    { 6, "A mean lord exiles fairytale creatures to the swamp of a grumpy ogre, who must go on a quest and rescue a princess for the lord in order to get his land back.", new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(8064), null },
                    { 7, "From personal healing, inspiration grew to create a positive film about people addressing disease with food. There are a lot of negative messages about the state of our nation's health and diet, and we were inspired by the community of people we found who are fighting back against this downward trend. Food As Medicine is a documentary film that follows the growing movement of using food to heal chronic illness and disease.", new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(8149), null },
                    { 8, "A faded television actor and his stunt double strive to achieve fame and success in the film industry during the final years of Hollywood's Golden Age in 1969 Los Angeles.", new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(8246), null },
                    { 9, "The U.S. government decides to go after an agro-business giant with a price-fixing accusation, based on the evidence submitted by their star witness, vice president-turned-informant Mark Whitacre.", new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(8331), null },
                    { 3, "The 1960s was the time of Beatles and Rolling Stones, the time of sexual revolution. These events have their echo in Bulgarian English-learning school. The school order provokes a protest of the students due to the narrow-minded teachers.", new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(7359), null },
                    { 11, "Disgruntled Korean War veteran Walt Kowalski sets out to reform his neighbor, a Hmong teenager who tried to steal Kowalski's prized possession: a 1972 Gran Torino.", new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(8654), null },
                    { 1, "A failed reporter is bonded to an alien entity, one of many symbiotes who have invaded Earth. But the being takes a liking to Earth and decides to protect it.", new DateTime(2019, 5, 10, 16, 26, 11, 11, DateTimeKind.Local).AddTicks(3148), null },
                    { 10, "Eight years after the Joker's reign of anarchy, Batman, with the help of the enigmatic Catwoman, is forced from his exile to save Gotham City, now on the edge of total annihilation, from the brutal guerrilla terrorist Bane.", new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(8425), null },
                    { 2, "Based on the true story of�Jordan Belfort, from his rise to a wealthy stock-broker living the high life to his fall involving crime, corruption and the federal government.", new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(7040), null }
                });

            migrationBuilder.InsertData(
                table: "Reviews",
                columns: new[] { "Id", "Content", "CreatedOn", "MovieId" },
                values: new object[,]
                {
                    { 5, "I hate romantic comedies. I detest them. You can list the actors I avoid watching: Hugh Grant, Sandra Bullock, Richard Gere, Julia Roberts. Romantic comedies make me cringe and I avoid them like the plague so you can imagine the foul mood I was in when I was forced to watch this film.", new DateTime(2019, 5, 10, 16, 26, 11, 42, DateTimeKind.Local).AddTicks(8336), null },
                    { 4, "Simply incredible. Never before have I seen a 3 hour movie that didn't seem like 3 hours. I read the Lord of the Rings very recently and I was surprised at how similar Peter Jackson's vision was to my own.", new DateTime(2019, 5, 10, 16, 26, 11, 42, DateTimeKind.Local).AddTicks(8054), null },
                    { 3, "I have been a cinema lover for years, read a lot of reviews on IMDb and everywhere, and never found the right movie to write my first review. I always thought I would wait for THE movie.", new DateTime(2019, 5, 10, 16, 26, 11, 42, DateTimeKind.Local).AddTicks(7876), null },
                    { 2, "In the mid-1990s, Jordan Belfort (Leonardo DiCaprio) and the rest of his associates from brokerage firm Stratton Oakmont became the very definition of excess and debauchery, their offices a boiler room fueled by cocaine and greed.", new DateTime(2019, 5, 10, 16, 26, 11, 42, DateTimeKind.Local).AddTicks(7546), null },
                    { 1, "We just came out of the theatre and I can honestly say. I liked it! It was funny and it paid great homage to the Todd McFarlane bombastic Venom era of the late 90's.", new DateTime(2019, 5, 10, 16, 26, 11, 39, DateTimeKind.Local).AddTicks(9005), null }
                });

            migrationBuilder.InsertData(
                table: "Genres",
                columns: new[] { "Id", "MainGenreId", "Type" },
                values: new object[,]
                {
                    { 4, 1, "Comedy" },
                    { 21, 3, "Computer animation" },
                    { 20, 3, "Stop motion" },
                    { 19, 3, "Traditional animation" },
                    { 17, 2, "Natural sciences" },
                    { 16, 2, "Technologies" },
                    { 15, 2, "Space" },
                    { 14, 2, "Medicine" },
                    { 13, 2, "Investigasion" },
                    { 18, 2, "Science fiction" },
                    { 11, 1, "Crime" },
                    { 10, 1, "Fantasy" },
                    { 9, 1, "Western" },
                    { 8, 1, "Horror" },
                    { 7, 1, "Thriller" },
                    { 6, 1, "Action" },
                    { 5, 1, "Drama" },
                    { 12, 2, "Historical" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "DirectorId", "Duration", "ImageURL", "MainGenreId", "PreviewId", "Rating", "ReleaseDate", "Title", "UserRating", "Voters" },
                values: new object[,]
                {
                    { 33, 6, 108, "\\images\\moviesImage\\informant_keyart.jpg", 1, 9, 6, new DateTime(2009, 10, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Informant", null, null },
                    { 32, 2, 165, "\\images\\moviesImage\\Screen-Shot-2019-03-18-at-10.40.42-AM.png", 1, 8, null, new DateTime(2019, 8, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "Once upon a time in Hollywood", null, null },
                    { 31, 5, 87, "\\images\\moviesImage\\download.jpg", 2, 7, 7, new DateTime(2016, 7, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Food as medicine", null, null },
                    { 30, 4, 90, "\\images\\moviesImage\\5081b38b05be3bed15ff10ead3d8e4ff.jpg", 3, 6, 8, new DateTime(2001, 10, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), "Shrek", null, null },
                    { 2, 7, 180, "\\images\\moviesImage\\maxresdefault.jpg", 1, 2, 9, new DateTime(2013, 12, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Wolf of Wall Street", null, null },
                    { 4, 9, 169, "\\images\\moviesImage\\Interstellar3.jpg", 1, 4, 8, new DateTime(2014, 11, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Interstellar", null, null },
                    { 3, 8, 84, "\\images\\moviesImage\\1390841477_9_800x_.jpg", 1, 3, 10, new DateTime(1988, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Yesterday", null, null },
                    { 1, 6, 112, "\\images\\moviesImage\\share.jpg", 1, 1, 6, new DateTime(2018, 10, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Venom", null, null },
                    { 34, 9, 164, "\\images\\moviesImage\\KFPOar1.jpg", 1, 10, 8, new DateTime(2012, 7, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Dark Knight Rises", null, null },
                    { 5, 10, 178, "\\images\\moviesImage\\Ringstrilogyposter.jpg", 1, 5, 9, new DateTime(2002, 3, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "The lord of the rings", null, null },
                    { 35, 1, 116, "\\images\\moviesImage\\wp2196783.jpg", 1, 11, 8, new DateTime(2009, 1, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "Gran Torino", null, null }
                });

            migrationBuilder.InsertData(
                table: "MoviesActors",
                columns: new[] { "MovieId", "ActorId" },
                values: new object[,]
                {
                    { 1, 6 },
                    { 32, 9 },
                    { 31, 17 },
                    { 30, 16 },
                    { 30, 15 },
                    { 35, 18 },
                    { 33, 5 },
                    { 5, 14 },
                    { 5, 13 },
                    { 32, 1 },
                    { 4, 3 },
                    { 4, 12 },
                    { 3, 11 },
                    { 3, 10 },
                    { 34, 4 },
                    { 34, 6 },
                    { 2, 9 },
                    { 2, 8 },
                    { 2, 2 },
                    { 1, 7 },
                    { 32, 2 }
                });

            migrationBuilder.InsertData(
                table: "MoviesGenres",
                columns: new[] { "MovieId", "GenreId" },
                values: new object[,]
                {
                    { 33, 4 },
                    { 34, 6 },
                    { 32, 5 },
                    { 34, 7 },
                    { 32, 4 },
                    { 33, 11 },
                    { 5, 10 },
                    { 30, 21 },
                    { 5, 5 },
                    { 4, 5 },
                    { 3, 5 },
                    { 2, 11 },
                    { 2, 5 },
                    { 1, 7 },
                    { 1, 6 },
                    { 31, 14 },
                    { 35, 5 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Genres_MainGenreId",
                table: "Genres",
                column: "MainGenreId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_DirectorId",
                table: "Movies",
                column: "DirectorId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_MainGenreId",
                table: "Movies",
                column: "MainGenreId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_PreviewId",
                table: "Movies",
                column: "PreviewId",
                unique: true,
                filter: "[PreviewId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_MoviesActors_ActorId",
                table: "MoviesActors",
                column: "ActorId");

            migrationBuilder.CreateIndex(
                name: "IX_MoviesGenres_GenreId",
                table: "MoviesGenres",
                column: "GenreId");

            migrationBuilder.CreateIndex(
                name: "IX_Reviews_MovieId",
                table: "Reviews",
                column: "MovieId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "MoviesActors");

            migrationBuilder.DropTable(
                name: "MoviesGenres");

            migrationBuilder.DropTable(
                name: "Reviews");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Actors");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Directors");

            migrationBuilder.DropTable(
                name: "Genres");

            migrationBuilder.DropTable(
                name: "Previews");
        }
    }
}
