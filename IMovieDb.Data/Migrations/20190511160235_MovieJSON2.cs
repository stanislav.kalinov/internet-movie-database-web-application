﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IMovieDb.Data.Migrations
{
    public partial class MovieJSON2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3,
                column: "ImageURL",
                value: "\\images\\newImage\\poster-fAaCImHbw8dGcL0vZHVKUKql0rx.jpg");

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 614, DateTimeKind.Local).AddTicks(2321));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(4581));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(4816));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(4903));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(4968));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(5196));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(5272));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(5351));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 9,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(5419));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 10,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(5489));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 11,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(5558));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 635, DateTimeKind.Local).AddTicks(2334));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 637, DateTimeKind.Local).AddTicks(3462));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 637, DateTimeKind.Local).AddTicks(3698));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 637, DateTimeKind.Local).AddTicks(3832));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 637, DateTimeKind.Local).AddTicks(3922));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3,
                column: "ImageURL",
                value: @"\images
ewImage\poster-fAaCImHbw8dGcL0vZHVKUKql0rx.jpg");

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 687, DateTimeKind.Local).AddTicks(2390));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(5336));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(5561));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(5648));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(5714));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(5945));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(6017));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(6091));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 9,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(6157));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 10,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(6228));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 11,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(6298));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 708, DateTimeKind.Local).AddTicks(2135));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 710, DateTimeKind.Local).AddTicks(3365));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 710, DateTimeKind.Local).AddTicks(3639));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 710, DateTimeKind.Local).AddTicks(3772));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 710, DateTimeKind.Local).AddTicks(3855));
        }
    }
}
