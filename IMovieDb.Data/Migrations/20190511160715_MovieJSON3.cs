﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IMovieDb.Data.Migrations
{
    public partial class MovieJSON3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5,
                column: "ImageURL",
                value: "\\images\\newImage\\1436084603512033650.jpg");

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 966, DateTimeKind.Local).AddTicks(6361));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 972, DateTimeKind.Local).AddTicks(9821));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 973, DateTimeKind.Local).AddTicks(64));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 973, DateTimeKind.Local).AddTicks(154));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 973, DateTimeKind.Local).AddTicks(225));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 973, DateTimeKind.Local).AddTicks(457));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 973, DateTimeKind.Local).AddTicks(536));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 973, DateTimeKind.Local).AddTicks(620));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 9,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 973, DateTimeKind.Local).AddTicks(691));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 10,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 973, DateTimeKind.Local).AddTicks(770));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 11,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 973, DateTimeKind.Local).AddTicks(849));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 987, DateTimeKind.Local).AddTicks(9270));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 990, DateTimeKind.Local).AddTicks(244));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 990, DateTimeKind.Local).AddTicks(482));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 990, DateTimeKind.Local).AddTicks(616));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 7, 13, 990, DateTimeKind.Local).AddTicks(701));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5,
                column: "ImageURL",
                value: "\\images\\newImage\\51Aqk8V+ZlL.jpg");

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 614, DateTimeKind.Local).AddTicks(2321));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(4581));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(4816));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(4903));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(4968));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(5196));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(5272));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(5351));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 9,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(5419));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 10,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(5489));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 11,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 620, DateTimeKind.Local).AddTicks(5558));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 635, DateTimeKind.Local).AddTicks(2334));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 637, DateTimeKind.Local).AddTicks(3462));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 637, DateTimeKind.Local).AddTicks(3698));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 637, DateTimeKind.Local).AddTicks(3832));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 2, 34, 637, DateTimeKind.Local).AddTicks(3922));
        }
    }
}
