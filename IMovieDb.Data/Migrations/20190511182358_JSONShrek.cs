﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IMovieDb.Data.Migrations
{
    public partial class JSONShrek : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Actors",
                columns: new[] { "Id", "Birthday", "FirstName", "LastName" },
                values: new object[,]
                {
                    { 14, new DateTime(1977, 1, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), "Orlando", "Bloom" },
                    { 18, new DateTime(1930, 5, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "Clint", "Eastwood" },
                    { 17, new DateTime(1977, 6, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sarah", "Ballantyne" },
                    { 16, new DateTime(1972, 8, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "Cameron", "Diaz" },
                    { 15, new DateTime(1961, 4, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Eddie", "Murphy" },
                    { 13, new DateTime(1981, 1, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Elijah", "Wood" },
                    { 12, new DateTime(1969, 11, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Matthew", "McConaughy" },
                    { 11, new DateTime(1964, 8, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Georgi", "Staikov" },
                    { 10, new DateTime(1964, 1, 4, 0, 0, 0, 0, DateTimeKind.Unspecified), "Hristo", "Shopov" },
                    { 9, new DateTime(1990, 7, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Margot", "Robbie" },
                    { 7, new DateTime(1980, 9, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "Michelle", "Williams" },
                    { 8, new DateTime(1983, 12, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jonah", "Hill" },
                    { 5, new DateTime(1978, 8, 6, 0, 0, 0, 0, DateTimeKind.Unspecified), "Matt", "Damon" },
                    { 4, new DateTime(1977, 6, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Christian", "Bale" },
                    { 3, new DateTime(1979, 4, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Jessica", "Chastain" },
                    { 2, new DateTime(1971, 11, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Leonardo", "DiCaprio" },
                    { 1, new DateTime(1967, 6, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Brad", "Pitt" },
                    { 6, new DateTime(1977, 9, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tom", "Hardy" }
                });

            migrationBuilder.InsertData(
                table: "Directors",
                columns: new[] { "Id", "FirstName", "LastName" },
                values: new object[,]
                {
                    { 5, "Lenore", "Eklund" },
                    { 6, "Ruben", "Fleischer" },
                    { 7, "Martin", "Scorsese" },
                    { 8, "Ivan", "Andonov" },
                    { 9, "Christopher", "Nolan" },
                    { 10, "Peter", "Jackson" },
                    { 4, "Andrew", "Adamson" },
                    { 1, "Clint", "Eastwood" },
                    { 2, "Quentin", "Tarantino" },
                    { 3, "Steven", "Soderbergh" }
                });

            migrationBuilder.InsertData(
                table: "Genres",
                columns: new[] { "Id", "MainGenreId", "Type" },
                values: new object[,]
                {
                    { 1, null, "Feature film" },
                    { 2, null, "Documentary" },
                    { 3, null, "Animation" }
                });

            migrationBuilder.InsertData(
                table: "Previews",
                columns: new[] { "Id", "Content", "CreatedOn", "MovieId" },
                values: new object[,]
                {
                    { 5, "A meek Hobbit from the Shire and eight companions set out on a journey to destroy the powerful One Ring and save Middle-earth from the Dark Lord Sauron.", new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(5609), null },
                    { 4, "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.", new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(5542), null },
                    { 6, "A mean lord exiles fairytale creatures to the swamp of a grumpy ogre, who must go on a quest and rescue a princess for the lord in order to get his land back.", new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(5708), null },
                    { 7, "From personal healing, inspiration grew to create a positive film about people addressing disease with food. There are a lot of negative messages about the state of our nation's health and diet, and we were inspired by the community of people we found who are fighting back against this downward trend. Food As Medicine is a documentary film that follows the growing movement of using food to heal chronic illness and disease.", new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(5776), null },
                    { 8, "A faded television actor and his stunt double strive to achieve fame and success in the film industry during the final years of Hollywood's Golden Age in 1969 Los Angeles.", new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(5854), null },
                    { 9, "The U.S. government decides to go after an agro-business giant with a price-fixing accusation, based on the evidence submitted by their star witness, vice president-turned-informant Mark Whitacre.", new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(6065), null },
                    { 3, "The 1960s was the time of Beatles and Rolling Stones, the time of sexual revolution. These events have their echo in Bulgarian English-learning school. The school order provokes a protest of the students due to the narrow-minded teachers.", new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(5448), null },
                    { 11, "Disgruntled Korean War veteran Walt Kowalski sets out to reform his neighbor, a Hmong teenager who tried to steal Kowalski's prized possession: a 1972 Gran Torino.", new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(6243), null },
                    { 1, "A failed reporter is bonded to an alien entity, one of many symbiotes who have invaded Earth. But the being takes a liking to Earth and decides to protect it.", new DateTime(2019, 5, 11, 21, 23, 57, 324, DateTimeKind.Local).AddTicks(2317), null },
                    { 10, "Eight years after the Joker's reign of anarchy, Batman, with the help of the enigmatic Catwoman, is forced from his exile to save Gotham City, now on the edge of total annihilation, from the brutal guerrilla terrorist Bane.", new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(6159), null },
                    { 2, "Based on the true story of�Jordan Belfort, from his rise to a wealthy stock-broker living the high life to his fall involving crime, corruption and the federal government.", new DateTime(2019, 5, 11, 21, 23, 57, 330, DateTimeKind.Local).AddTicks(5212), null }
                });

            migrationBuilder.InsertData(
                table: "Reviews",
                columns: new[] { "Id", "Content", "CreatedOn", "MovieId" },
                values: new object[,]
                {
                    { 5, "I hate romantic comedies. I detest them. You can list the actors I avoid watching: Hugh Grant, Sandra Bullock, Richard Gere, Julia Roberts. Romantic comedies make me cringe and I avoid them like the plague so you can imagine the foul mood I was in when I was forced to watch this film.", new DateTime(2019, 5, 11, 21, 23, 57, 347, DateTimeKind.Local).AddTicks(2695), null },
                    { 4, "Simply incredible. Never before have I seen a 3 hour movie that didn't seem like 3 hours. I read the Lord of the Rings very recently and I was surprised at how similar Peter Jackson's vision was to my own.", new DateTime(2019, 5, 11, 21, 23, 57, 347, DateTimeKind.Local).AddTicks(2608), null },
                    { 3, "I have been a cinema lover for years, read a lot of reviews on IMDb and everywhere, and never found the right movie to write my first review. I always thought I would wait for THE movie.", new DateTime(2019, 5, 11, 21, 23, 57, 347, DateTimeKind.Local).AddTicks(2471), null },
                    { 2, "In the mid-1990s, Jordan Belfort (Leonardo DiCaprio) and the rest of his associates from brokerage firm Stratton Oakmont became the very definition of excess and debauchery, their offices a boiler room fueled by cocaine and greed.", new DateTime(2019, 5, 11, 21, 23, 57, 347, DateTimeKind.Local).AddTicks(2236), null },
                    { 1, "We just came out of the theatre and I can honestly say. I liked it! It was funny and it paid great homage to the Todd McFarlane bombastic Venom era of the late 90's.", new DateTime(2019, 5, 11, 21, 23, 57, 345, DateTimeKind.Local).AddTicks(1213), null }
                });

            migrationBuilder.InsertData(
                table: "Genres",
                columns: new[] { "Id", "MainGenreId", "Type" },
                values: new object[,]
                {
                    { 4, 1, "Comedy" },
                    { 21, 3, "Computer animation" },
                    { 20, 3, "Stop motion" },
                    { 19, 3, "Traditional animation" },
                    { 17, 2, "Natural sciences" },
                    { 16, 2, "Technologies" },
                    { 15, 2, "Space" },
                    { 14, 2, "Medicine" },
                    { 13, 2, "Investigasion" },
                    { 18, 2, "Science fiction" },
                    { 11, 1, "Crime" },
                    { 10, 1, "Fantasy" },
                    { 9, 1, "Western" },
                    { 8, 1, "Horror" },
                    { 7, 1, "Thriller" },
                    { 6, 1, "Action" },
                    { 5, 1, "Drama" },
                    { 12, 2, "Historical" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "DirectorId", "Duration", "ImageURL", "MainGenreId", "PreviewId", "Rating", "ReleaseDate", "Title", "UserRating", "Voters" },
                values: new object[,]
                {
                    { 33, 6, 108, "\\images\\moviesImage\\informant_keyart.jpg", 1, 9, 6, new DateTime(2009, 10, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Informant", null, null },
                    { 32, 2, 165, "\\images\\newImage\\once_upon_a_time_in_hollywood_fan_poster_by_nicksplosivez_dc56txq-pre.jpg", 1, 8, null, new DateTime(2019, 8, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "Once upon a time in Hollywood", null, null },
                    { 31, 5, 87, "\\images\\newImage\\flat,550x550,075,f.u4.jpg", 2, 7, 7, new DateTime(2016, 7, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Food as medicine", null, null },
                    { 30, 4, 90, "\\images\\newImage\\81QYuLmQT1L._SY679_.jpg", 3, 6, 8, new DateTime(2001, 10, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), "Shrek", null, null },
                    { 2, 7, 180, "\\images\\newImage\\mHP0138_1024x1024.jpeg", 1, 2, 9, new DateTime(2013, 12, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Wolf of Wall Street", null, null },
                    { 4, 9, 169, "\\images\\newImage\\MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_.jpg", 1, 4, 8, new DateTime(2014, 11, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Interstellar", null, null },
                    { 3, 8, 84, "\\images\\newImage\\poster-fAaCImHbw8dGcL0vZHVKUKql0rx.jpg", 1, 3, 10, new DateTime(1988, 1, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Yesterday", null, null },
                    { 1, 6, 112, "\\images\\newImage\\MV5BNzAwNzUzNjY4MV5BMl5BanBnXkFtZTgwMTQ5MzM0NjM@._V1_UY1200_CR90,0,630,1200_AL_.jpg", 1, 1, 6, new DateTime(2018, 10, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Venom", null, null },
                    { 34, 9, 164, "\\images\\newImage\\baaf0a93b13dde0422d2ced6c81135b2.jpg", 1, 10, 8, new DateTime(2012, 7, 27, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Dark Knight Rises", null, null },
                    { 5, 10, 178, "\\images\\newImage\\1436084603512033650.jpg", 1, 5, 9, new DateTime(2002, 3, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "The lord of the rings", null, null },
                    { 35, 1, 116, "\\images\\newImage\\gran_torino_by_skitzofranik27-d34rvhr.jpg", 1, 11, 8, new DateTime(2009, 1, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "Gran Torino", null, null }
                });

            migrationBuilder.InsertData(
                table: "MoviesActors",
                columns: new[] { "MovieId", "ActorId" },
                values: new object[,]
                {
                    { 1, 6 },
                    { 32, 9 },
                    { 31, 17 },
                    { 30, 16 },
                    { 30, 15 },
                    { 35, 18 },
                    { 33, 5 },
                    { 5, 14 },
                    { 5, 13 },
                    { 32, 1 },
                    { 4, 3 },
                    { 4, 12 },
                    { 3, 11 },
                    { 3, 10 },
                    { 34, 4 },
                    { 34, 6 },
                    { 2, 9 },
                    { 2, 8 },
                    { 2, 2 },
                    { 1, 7 },
                    { 32, 2 }
                });

            migrationBuilder.InsertData(
                table: "MoviesGenres",
                columns: new[] { "MovieId", "GenreId" },
                values: new object[,]
                {
                    { 33, 4 },
                    { 34, 6 },
                    { 32, 5 },
                    { 34, 7 },
                    { 32, 4 },
                    { 33, 11 },
                    { 5, 10 },
                    { 30, 21 },
                    { 5, 5 },
                    { 4, 5 },
                    { 3, 5 },
                    { 2, 11 },
                    { 2, 5 },
                    { 1, 7 },
                    { 1, 6 },
                    { 31, 14 },
                    { 35, 5 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Directors",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 1, 6 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 1, 7 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 2, 8 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 2, 9 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 3, 10 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 3, 11 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 4, 3 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 4, 12 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 5, 13 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 5, 14 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 30, 15 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 30, 16 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 31, 17 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 32, 1 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 32, 2 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 32, 9 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 33, 5 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 34, 4 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 34, 6 });

            migrationBuilder.DeleteData(
                table: "MoviesActors",
                keyColumns: new[] { "MovieId", "ActorId" },
                keyValues: new object[] { 35, 18 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 1, 6 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 1, 7 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 2, 5 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 2, 11 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 3, 5 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 4, 5 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 5, 5 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 5, 10 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 30, 21 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 31, 14 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 32, 4 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 32, 5 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 33, 4 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 33, 11 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 34, 6 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 34, 7 });

            migrationBuilder.DeleteData(
                table: "MoviesGenres",
                keyColumns: new[] { "MovieId", "GenreId" },
                keyValues: new object[] { 35, 5 });

            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Actors",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Directors",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Directors",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Directors",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Directors",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Directors",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Directors",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Directors",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Directors",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Directors",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Genres",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 11);
        }
    }
}
