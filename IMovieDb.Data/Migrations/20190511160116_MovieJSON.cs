﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IMovieDb.Data.Migrations
{
    public partial class MovieJSON : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1,
                column: "ImageURL",
                value: "\\images\\newImage\\MV5BNzAwNzUzNjY4MV5BMl5BanBnXkFtZTgwMTQ5MzM0NjM@._V1_UY1200_CR90,0,630,1200_AL_.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2,
                column: "ImageURL",
                value: "\\images\\newImage\\mHP0138_1024x1024.jpeg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3,
                column: "ImageURL",
                value: @"\images
ewImage\poster-fAaCImHbw8dGcL0vZHVKUKql0rx.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4,
                column: "ImageURL",
                value: "\\images\\newImage\\MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5,
                column: "ImageURL",
                value: "\\images\\newImage\\51Aqk8V+ZlL.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 30,
                column: "ImageURL",
                value: "\\images\\newImage\\2eb3f12fb9e2d956c97be7afdab5865e.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 31,
                column: "ImageURL",
                value: "\\images\\newImage\\flat,550x550,075,f.u4.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 32,
                column: "ImageURL",
                value: "\\images\\newImage\\once_upon_a_time_in_hollywood_fan_poster_by_nicksplosivez_dc56txq-pre.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 34,
                column: "ImageURL",
                value: "\\images\\newImage\\baaf0a93b13dde0422d2ced6c81135b2.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 35,
                column: "ImageURL",
                value: "\\images\\newImage\\gran_torino_by_skitzofranik27-d34rvhr.jpg");

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 687, DateTimeKind.Local).AddTicks(2390));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(5336));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(5561));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(5648));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(5714));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(5945));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(6017));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(6091));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 9,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(6157));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 10,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(6228));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 11,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 693, DateTimeKind.Local).AddTicks(6298));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 708, DateTimeKind.Local).AddTicks(2135));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 710, DateTimeKind.Local).AddTicks(3365));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 710, DateTimeKind.Local).AddTicks(3639));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 710, DateTimeKind.Local).AddTicks(3772));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 11, 19, 1, 14, 710, DateTimeKind.Local).AddTicks(3855));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1,
                column: "ImageURL",
                value: "\\images\\moviesImage\\share.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2,
                column: "ImageURL",
                value: "\\images\\moviesImage\\maxresdefault.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3,
                column: "ImageURL",
                value: "\\images\\moviesImage\\1390841477_9_800x_.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4,
                column: "ImageURL",
                value: "\\images\\moviesImage\\Interstellar3.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5,
                column: "ImageURL",
                value: "\\images\\moviesImage\\Ringstrilogyposter.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 30,
                column: "ImageURL",
                value: "\\images\\moviesImage\\5081b38b05be3bed15ff10ead3d8e4ff.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 31,
                column: "ImageURL",
                value: "\\images\\moviesImage\\download.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 32,
                column: "ImageURL",
                value: "\\images\\moviesImage\\Screen-Shot-2019-03-18-at-10.40.42-AM.png");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 34,
                column: "ImageURL",
                value: "\\images\\moviesImage\\KFPOar1.jpg");

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 35,
                column: "ImageURL",
                value: "\\images\\moviesImage\\wp2196783.jpg");

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 11, DateTimeKind.Local).AddTicks(3148));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(7040));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(7359));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(7826));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(7929));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 6,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(8064));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 7,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(8149));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 8,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(8246));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 9,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(8331));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 10,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(8425));

            migrationBuilder.UpdateData(
                table: "Previews",
                keyColumn: "Id",
                keyValue: 11,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 19, DateTimeKind.Local).AddTicks(8654));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 39, DateTimeKind.Local).AddTicks(9005));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 42, DateTimeKind.Local).AddTicks(7546));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 42, DateTimeKind.Local).AddTicks(7876));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 42, DateTimeKind.Local).AddTicks(8054));

            migrationBuilder.UpdateData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: 5,
                column: "CreatedOn",
                value: new DateTime(2019, 5, 10, 16, 26, 11, 42, DateTimeKind.Local).AddTicks(8336));
        }
    }
}
