﻿using IMovieDb.Data.Configurations;
using IMovieDb.Data.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MovieDb.Data.Configurations;
using MovieDb.Data.Models;
using Newtonsoft.Json;
using System.IO;
using System.Linq;

namespace MovieDb.Data.Context
{
    public class MovieDbContext : IdentityDbContext<ApplicationUser>
    {
        public MovieDbContext(DbContextOptions<MovieDbContext> options)
            : base(options)
        {
        }

        public MovieDbContext()
        {
        }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Director> Directors { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Preview> Previews { get; set; }
        public DbSet<MoviesActors> MoviesActors { get; set; }
        public DbSet<MoviesGenres> MoviesGenres { get; set; }
        public DbSet<Review> Reviews { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //this.SeedJSONData(modelBuilder);

            modelBuilder.ApplyConfiguration(new ActorConfiguration());
            modelBuilder.ApplyConfiguration(new DirectorConfiguration());
            modelBuilder.ApplyConfiguration(new GenreConfiguration());
            modelBuilder.ApplyConfiguration(new MovieConfiguration());
            modelBuilder.ApplyConfiguration(new MoviesActorsConfiguration());
            modelBuilder.ApplyConfiguration(new MoviesGenresConfiguration());
            modelBuilder.ApplyConfiguration(new PreviewConfiguration());
            modelBuilder.ApplyConfiguration(new ReviewConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        private void SeedJSONData(ModelBuilder modelBuilder)
        {
            var movies = File.ReadAllText(@"..\IMovieDb.Data\JSON.Data\Movies.json");
            var directors = File.ReadAllText(@"..\IMovieDb.Data\JSON.Data\Directors.json");
            var actors = File.ReadAllText(@"..\IMovieDb.Data\JSON.Data\Actors.json");
            var genres = File.ReadAllText(@"..\IMovieDb.Data\JSON.Data\Genres.json");
            var previews = File.ReadAllText(@"..\IMovieDb.Data\JSON.Data\Previews.json");
            var moviesActors = File.ReadAllText(@"..\IMovieDb.Data\JSON.Data\MoviesActors.json");
            var moviesGenres = File.ReadAllText(@"..\IMovieDb.Data\JSON.Data\MoviesGenres.json");
            var reviews = File.ReadAllText(@"..\\IMovieDb.Data\JSON.Data\Reviews.json");

            var jsonMovies = JsonConvert.DeserializeObject<Movie[]>(movies);
            var jsonDirectors = JsonConvert.DeserializeObject<Director[]>(directors);
            var jsonActors = JsonConvert.DeserializeObject<Actor[]>(actors);
            var jsonGenres = JsonConvert.DeserializeObject<Genre[]>(genres);
            var jsonPreviews = JsonConvert.DeserializeObject<Preview[]>(previews);
            var jsonMoviesActors = JsonConvert.DeserializeObject<MoviesActors[]>(moviesActors);
            var jsonMoviesGenres = JsonConvert.DeserializeObject<MoviesGenres[]>(moviesGenres);
            var jsonReviews = JsonConvert.DeserializeObject<Review[]>(reviews);


            modelBuilder.Entity<Director>().HasData(jsonDirectors);
            modelBuilder.Entity<Actor>().HasData(jsonActors);
            modelBuilder.Entity<Genre>().HasData(jsonGenres);
            modelBuilder.Entity<Preview>().HasData(jsonPreviews);
            modelBuilder.Entity<Movie>().HasData(jsonMovies);
            modelBuilder.Entity<MoviesActors>().HasData(jsonMoviesActors);
            modelBuilder.Entity<MoviesGenres>().HasData(jsonMoviesGenres);
            modelBuilder.Entity<Review>().HasData(jsonReviews);
        }

        public static void SeedUserAdmin(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var dbContext = scope.ServiceProvider.GetRequiredService<MovieDbContext>();

                if (dbContext.Roles.Any(u => u.Name == "Admin"))
                {
                    return;
                }

                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                roleManager.CreateAsync(new IdentityRole { Name = "Admin" }).Wait();

                var adminUser = new ApplicationUser { UserName = "admin@admin.com", Email = "admin@admin.com" };
                userManager.CreateAsync(adminUser, "Admin123@").Wait();

                userManager.AddToRoleAsync(adminUser, "Admin").Wait();
            }
        }
    }
}
