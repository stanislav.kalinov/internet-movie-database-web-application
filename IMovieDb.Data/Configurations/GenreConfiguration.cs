﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieDb.Data.Models;

namespace MovieDb.Data.Configurations
{
    public class GenreConfiguration : IEntityTypeConfiguration<Genre>
    {
        public void Configure(EntityTypeBuilder<Genre> builder)
        {
            builder.HasKey(g => g.Id);

            builder.Property(g => g.Type)
                .HasMaxLength(25)
                .IsRequired();

            builder
                .HasOne(g => g.MainGenre)
                .WithMany(pg => pg.ChildGenres)
                .HasForeignKey(g => g.MainGenreId);
        }
    }
}
