﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieDb.Data.Models;

namespace MovieDb.Data.Configurations
{
    public class DirectorConfiguration : IEntityTypeConfiguration<Director>
    {
        public void Configure(EntityTypeBuilder<Director> builder)
        {
            builder.HasKey(d => d.Id);

            builder.Property(d => d.FirstName)
                .HasMaxLength(15)
                .IsRequired();

            builder.Property(d => d.LastName)
                .HasMaxLength(15)
                .IsRequired();
        }
    }
}
