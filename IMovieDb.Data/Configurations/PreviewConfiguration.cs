﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieDb.Data.Models;

namespace MovieDb.Data.Configurations
{
    public class PreviewConfiguration : IEntityTypeConfiguration<Preview>
    {
        public void Configure(EntityTypeBuilder<Preview> builder)
        {
            builder.HasKey(p => p.Id);

            builder.Property(p => p.Content)
                .HasColumnType("ntext")
                .IsRequired();

            builder.Property(p => p.CreatedOn)
                .IsRequired();
        }
    }
}
