﻿using IMovieDb.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace IMovieDb.Data.Configurations
{
    public class ReviewConfiguration : IEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> builder)
        {
            builder.HasKey(r => r.Id);

            builder.Property(r => r.Content)
                .HasColumnType("ntext")
                .IsRequired();

            builder.Property(r => r.CreatedOn)
                .IsRequired();

            builder
              .HasOne(r => r.Movie)
              .WithMany(m => m.Reviews)
              .HasForeignKey(r => r.MovieId);
        }
    }
}
