﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieDb.Data.Models;

namespace MovieDb.Data.Configurations
{
    public class MovieConfiguration : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.HasKey(m => m.Id);

            builder.Property(m => m.Title)
                .HasMaxLength(30)
                .IsRequired();

            builder.Property(m => m.ReleaseDate)
                .IsRequired();

            builder.Property(m => m.Duration)
                .IsRequired();

            builder
                .HasOne(m => m.Director)
                .WithMany(d => d.Movies)
                .HasForeignKey(m => m.DirectorId)
                .OnDelete(DeleteBehavior.SetNull);

            builder
                .HasOne(m => m.Preview)
                .WithOne(p => p.Movie)
                .HasForeignKey<Movie>(m => m.PreviewId)
                .OnDelete(DeleteBehavior.SetNull);

            builder
                .HasOne(m => m.MainGenre)
                .WithMany(g => g.MainGenreMovies)
                .HasForeignKey(m => m.MainGenreId)
                .OnDelete(DeleteBehavior.SetNull);

        }
    }
}
