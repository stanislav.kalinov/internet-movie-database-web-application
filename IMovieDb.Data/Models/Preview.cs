﻿using System;

namespace MovieDb.Data.Models
{
    public class Preview
    {
        public Preview()
        {
            this.CreatedOn = DateTime.Now;
        }

        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? MovieId { get; set; }
        public Movie Movie { get; set; }
    }
}
