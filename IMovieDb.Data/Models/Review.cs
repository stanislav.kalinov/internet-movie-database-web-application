﻿using MovieDb.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace IMovieDb.Data.Models
{
    public class Review
    {
        public Review()
        {
            this.CreatedOn = DateTime.Now;
        }

        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? MovieId { get; set; }
        public Movie Movie { get; set; }
    }
}
