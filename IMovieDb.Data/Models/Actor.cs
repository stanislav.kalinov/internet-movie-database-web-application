﻿using System;
using System.Collections.Generic;

namespace MovieDb.Data.Models
{
    public class Actor
    {
        public Actor()
        {
            this.MoviesActors = new HashSet<MoviesActors>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? Birthday { get; set; }

        public ICollection<MoviesActors> MoviesActors { get; set; }
    }
}
