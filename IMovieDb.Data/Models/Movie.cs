﻿using IMovieDb.Data.Models;
using System;
using System.Collections.Generic;

namespace MovieDb.Data.Models
{
    public class Movie
    {
        public Movie()
        {
            this.MoviesActors = new HashSet<MoviesActors>();
            this.MoviesGenres = new HashSet<MoviesGenres>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public int Duration { get; set; }
        public string ImageURL { get; set; }
        public int? Rating { get; set; }
        public decimal? UserRating { get; set; }
        public int? Voters { get; set; }

        public int? DirectorId { get; set; }
        public Director Director { get; set; }

        public int? PreviewId { get; set; }
        public Preview Preview { get; set; }

        public int? MainGenreId { get; set; }
        public Genre MainGenre { get; set; }

        public ICollection<MoviesActors> MoviesActors { get; set; }

        public ICollection<MoviesGenres> MoviesGenres { get; set; }

        public ICollection<Review> Reviews { get; set; }
    }
}
