﻿using System.Collections.Generic;

namespace MovieDb.Data.Models
{
    public class Genre
    {
        public Genre()
        {
            this.ChildGenres = new HashSet<Genre>();
            this.MoviesGenres = new HashSet<MoviesGenres>();
            this.MainGenreMovies = new HashSet<Movie>();
        }

        public int Id { get; set; }
        public string Type { get; set; }

        public int? MainGenreId { get; set; }
        public Genre MainGenre { get; set; }

        public ICollection<Genre> ChildGenres { get; set; }

        public ICollection<Movie> MainGenreMovies { get; set; }

        public ICollection<MoviesGenres> MoviesGenres { get; set; }
    }
}
