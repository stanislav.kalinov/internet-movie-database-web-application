﻿using Microsoft.AspNetCore.Identity;

namespace IMovieDb.Data.Models
{
    public class ApplicationUser : IdentityUser
    {

    }
}
