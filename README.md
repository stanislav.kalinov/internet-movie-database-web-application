# Internet Movie Database Web Application

***Team members:***

* Stanislav Kalinov 
* Lilyana Shapkadjieva

***Aim of the project:***

Design and implement an ASP.NET Core MVC application for management a database of movies with features for administration, searching and listing by different criteria.

***Application features:***

**The application has:**

* public part (accessible without authentication)
* private part (available for registered users)
* administrative part (available for administrators only)

Registered users have one of the two roles: user or administrator.

**The application uses:**

* ASP.NET Core MVC 2.2
* Razor template engine for generating the UI
* MS SQL Server as database back-end
* External JSON files to provide part of the data in the SQL Server 
* Entity Framework Core to access the database
* Code first approach to create the database schema
* Data validation (both client-side and server-side)
* Proper error handling (both client-side and server-side)
* Caching of data via custom middlware
* Azure App hosting services to host the application

**Functionalities:**

**Creating:**
* Add movie
* Add director to movie
* Add actor to movie
* Add movie preview
* Add movie rating
* Add movie genre
* Add main genre

**Changing:**
* Delete movie
* Delete movie genre
* Change movie preview
* Change movie rating

**Listing:**
* Show movie summary
* Show top rating movies
* Show movies by genre
* Show movies by actor
* Show movies by director
* Comming Soon
* Show movies by release year
* Show movies by release date


