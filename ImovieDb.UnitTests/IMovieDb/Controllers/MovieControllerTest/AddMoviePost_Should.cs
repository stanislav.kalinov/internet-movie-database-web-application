﻿using Core_2._2.Mappers.Contracts;
using Core_2._2.Models;
using IMovieDb.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MovieDb.Data.Models;
using MovieDb.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace IMovieDb.UnitTests.IMovieDb.Controllers.MovieControllerTest
{
    [TestClass]
    public class AddMoviePost_Should
    {
        [TestMethod]
        public async Task Return_RedirectToActionResult_WhenValidInputIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);

            var model = new MovieViewModel()
            {
                Title = "Titanic",
                ReleaseDate = DateTime.ParseExact("13/12/2006", "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("de-De")),
                Duration = 180
            };

            var movie = new Movie()
            {
                Title = model.Title,
                ReleaseDate = model.ReleaseDate,
                Duration = model.Duration
            };

            movieServiceMock
                .Setup(ms => ms.AddMovieAsync(model.Title, model.ReleaseDate, model.Duration))
                .ReturnsAsync(movie);

            //Act
            var result = await sut.AddMovie(model);

            //Assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        }

        [TestMethod]
        public async Task Return_RedirectToActionResult_WithValidParameters_WhenValidInputIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);

            var model = new MovieViewModel()
            {
                Title = "Titanic",
                ReleaseDate = DateTime.ParseExact("13/12/2006", "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("de-De")),
                Duration = 180
            };

            var movie = new Movie()
            {
                Title = model.Title,
                ReleaseDate = model.ReleaseDate,
                Duration = model.Duration
            };

            movieServiceMock
                .Setup(ms => ms.AddMovieAsync(model.Title, model.ReleaseDate, model.Duration))
                .ReturnsAsync(movie);

            //Act
            var result = await sut.AddMovie(model) as RedirectToActionResult;

            //Assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual("Home", result.ControllerName);
            Assert.AreEqual("Index", result.ActionName);
        }

        [TestMethod]
        public async Task Return_ViewResult_WhenInvalidModelIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);

            var model = new MovieViewModel();

            sut.ModelState.AddModelError("Error", "Error");            

            //Act
            var result = await sut.AddMovie(model);

            //Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public async Task Return_ViewResultWithModel_WhenInvalidModelIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);

            var model = new MovieViewModel();

            sut.ModelState.AddModelError("Error", "Error");

            //Act
            var result = await sut.AddMovie(model) as ViewResult;

            //Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(model, result.Model);
        }

        [TestMethod]
        public async Task Invoke_AddMovieAsyncMethod_WhenValidModelIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);


            string title = "Titanic";
            DateTime releaseDate = DateTime.Now;
            int duration = 180;


            var model = new MovieViewModel()
            {
                Title = title,
                ReleaseDate = releaseDate,
                Duration = duration,
            };

            movieServiceMock
                .Setup(ms => ms.AddMovieAsync(title, releaseDate, duration))
                .ReturnsAsync(new Movie());

            //Act
            var result = await sut.AddMovie(model);

            //Assert
            movieServiceMock.Verify(ms => ms.AddMovieAsync(title, releaseDate, duration), Times.Once);
        }

        [TestMethod]
        public async Task Invoke_AddGenreAsyncMethod_WhenValidModelIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);


            string title = "Titanic";
            DateTime releaseDate = DateTime.Now;
            int duration = 180;
            string genre = "Action";


            var model = new MovieViewModel()
            {
                Title = title,
                ReleaseDate = releaseDate,
                Duration = duration,
                MovieGenres = new List<string>() { genre }
            };

            genreServiceMock
                .Setup(gs => gs.AddGenreAsync(title, genre))
                .ReturnsAsync(new Genre());

            //Act
            var result = await sut.AddMovie(model);

            //Assert
            genreServiceMock.Verify(ms => ms.AddGenreAsync(title, genre), Times.Once);
        }

        [TestMethod]
        public async Task Invoke_AddDirectorAsyncMethod_WhenValidModelIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);


            string title = "Titanic";
            DateTime releaseDate = DateTime.Now;
            int duration = 180;
            string firstName = "Ivan";
            string lastName = "Ivanov";


            var model = new MovieViewModel()
            {
                Title = title,
                ReleaseDate = releaseDate,
                Duration = duration,
                Director = new Director() { FirstName = firstName, LastName = lastName }
            };

            directorServiceMock
                .Setup(ds => ds.AddDirectorAsync(title, firstName, lastName))
                .ReturnsAsync(new Director());

            //Act
            var result = await sut.AddMovie(model);

            //Assert
            directorServiceMock.Verify(ds => ds.AddDirectorAsync(title, firstName, lastName), Times.Once);
        }

        [TestMethod]
        public async Task Invoke_AddActorAsyncMethod_WhenValidModelIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);


            string title = "Titanic";
            DateTime releaseDate = DateTime.Now;
            int duration = 180;
            string firstName = "Ivan";
            string lastName = "Ivanov";
            var birthday = DateTime.Now;


            var actor = new Actor()
            {
                FirstName = firstName,
                LastName = lastName,
                Birthday = birthday
            };

            var model = new MovieViewModel()
            {
                Title = title,
                ReleaseDate = releaseDate,
                Duration = duration,
                Actors = new List<Actor>() { actor }
            };

            actorServiceMock
                .Setup(actorService => actorService.AddActorAsync(title, firstName, lastName, birthday))
                .ReturnsAsync(new Actor());

            //Act
            var result = await sut.AddMovie(model);

            //Assert
            actorServiceMock.Verify(actorService => actorService.AddActorAsync(title, firstName, lastName, birthday), Times.Once);
        }

        [TestMethod]
        public async Task Invoke_AddRatingAsyncMethod_WhenValidModelIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);


            string title = "Titanic";
            DateTime releaseDate = DateTime.Now;
            int duration = 180;
            int rating = 10;

            var model = new MovieViewModel()
            {
                Title = title,
                ReleaseDate = releaseDate,
                Duration = duration,
                Rating = rating
            };

            movieServiceMock
                .Setup(ms => ms.AddRatingAsync(title, rating))
                .ReturnsAsync(new Movie());

            //Act
            var result = await sut.AddMovie(model);

            //Assert
            movieServiceMock.Verify(ms => ms.AddRatingAsync(title, rating), Times.Once);
        }

        [TestMethod]
        public async Task Invoke_AddPreviewAsyncMethod_WhenValidModelIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);


            string title = "Titanic";
            DateTime releaseDate = DateTime.Now;
            int duration = 180;
            string preview = "Preview";

            var model = new MovieViewModel()
            {
                Title = title,
                ReleaseDate = releaseDate,
                Duration = duration,
                Preview = new Preview() { Content = preview }
            };

            previewServiceMock
                .Setup(ps => ps.AddPreviewAsync(title, preview))
                .ReturnsAsync(new Preview());

            //Act
            var result = await sut.AddMovie(model);

            //Assert
            previewServiceMock.Verify(ps => ps.AddPreviewAsync(title, preview), Times.Once);
        }
    }
}
