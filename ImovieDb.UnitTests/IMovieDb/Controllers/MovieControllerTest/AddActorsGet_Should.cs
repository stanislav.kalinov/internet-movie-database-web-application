﻿using Core_2._2.Mappers.Contracts;
using Core_2._2.Models;
using IMovieDb.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MovieDb.Data.Models;
using MovieDb.Services.Contracts;
using System.Collections.Generic;

namespace IMovieDb.UnitTests.IMovieDb.Controllers.MovieControllerTest
{
    [TestClass]
    public class AddActorsGet_Should
    {
        [TestMethod]
        public void Return_CorrectViewResult()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);

            //Act
            var result = sut.AddActors();
      
            //Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }
    }
}
