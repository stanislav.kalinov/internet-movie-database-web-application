﻿using Core_2._2.Mappers.Contracts;
using Core_2._2.Models;
using IMovieDb.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MovieDb.Data.Models;
using MovieDb.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IMovieDb.UnitTests.IMovieDb.Controllers.MovieControllerTest
{
    [TestClass]
    public class AddActorsPost_Should
    {
        [TestMethod]
        public async Task Return_RedirectToActionResult_WhenValidInputIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);

            string title = "Titanic";
            string firstName = "Ivan";
            string lastName = "Ivanov";
            DateTime birthday = DateTime.Now;
            var actor = new Actor() { FirstName = "Petko", LastName = "Petkov", Birthday = DateTime.Now };

            var model = new ActorsViewModel()
            {
                MovieTitle = title,
                FirstName = firstName,
                LastName = lastName,
                Birthday = birthday,
                Actors = new List<Actor> { actor }
            };

            actorServiceMock
                .Setup(ms => ms.AddActorAsync(title, firstName, lastName, birthday))
                .ReturnsAsync(new Actor());

            //Act
            var result = await sut.AddActors(model);

            //Assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
        }

        [TestMethod]
        public async Task Return_RedirectToActionResult_WithValidParameters_WhenValidInputIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);

            string title = "Titanic";
            string firstName = "Ivan";
            string lastName = "Ivanov";
            DateTime birthday = DateTime.Now;
            var actor = new Actor() { FirstName = "Petko", LastName = "Petkov", Birthday = DateTime.Now };

            var model = new ActorsViewModel()
            {
                MovieTitle = title,
                FirstName = firstName,
                LastName = lastName,
                Birthday = birthday,
                Actors = new List<Actor> { actor }
            };

            actorServiceMock
                .Setup(ms => ms.AddActorAsync(title, firstName, lastName, birthday))
                .ReturnsAsync(new Actor());

            //Act
            var result = await sut.AddActors(model) as RedirectToActionResult;

            //Assert
            Assert.IsInstanceOfType(result, typeof(RedirectToActionResult));
            Assert.AreEqual("Home", result.ControllerName);
            Assert.AreEqual("Index", result.ActionName);
        }

        [TestMethod]
        public async Task Return_ViewResult_WhenInvalidModelIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);

            var model = new ActorsViewModel();

            sut.ModelState.AddModelError("Error", "Error");

            //Act
            var result = await sut.AddActors(model);

            //Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public async Task Return_ViewResultWithModel_WhenInvalidModelIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);

            var model = new ActorsViewModel();

            sut.ModelState.AddModelError("Error", "Error");

            //Act
            var result = await sut.AddActors(model) as ViewResult;

            //Assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.AreEqual(model, result.Model);
        }



        [TestMethod]
        public async Task Invoke_AddActorAsyncMethod_WhenValidModelIsPassed()
        {
            //Arrange
            var movieServiceMock = new Mock<IMovieService>();
            var directorServiceMock = new Mock<IDirectorService>();
            var actorServiceMock = new Mock<IActorService>();
            var genreServiceMock = new Mock<IGenreService>();
            var previewServiceMock = new Mock<IPreviewService>();
            var movieMapperMock = new Mock<IViewModelMapper<Movie, MovieViewModel>>();
            var moviesMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>>();
            var searchMapperMock = new Mock<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>>();

            var sut = new MovieController(movieServiceMock.Object, directorServiceMock.Object, actorServiceMock.Object, moviesMapperMock.Object,
                                          genreServiceMock.Object, previewServiceMock.Object, movieMapperMock.Object, searchMapperMock.Object);


            string title = "Titanic";
            string firstName = "Ivan";
            string lastName = "Ivanov";
            DateTime birthday = DateTime.Now;
            var actor = new Actor() { FirstName = "Petko", LastName = "Petkov", Birthday = DateTime.Now };

            var model = new ActorsViewModel()
            {
                MovieTitle = title,
                FirstName = firstName,
                LastName = lastName,
                Birthday = birthday,
                Actors = new List<Actor> { actor }
            };

            actorServiceMock
                .Setup(actorsService => actorsService.AddActorAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>()))
                .ReturnsAsync(new Actor());

            //Act
            var result = await sut.AddActors(model);

            //Assert
            actorServiceMock.Verify(actorsService => actorsService.AddActorAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>()), Times.Exactly(2));
        }
    }
}
