﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MovieDb.Data.Context;

namespace IMovieDb.UnitTests.Common
{
    public static class TestUtils
    {
        public static DbContextOptions<MovieDbContext> GetOptions(string databaseName)
        {
            //var provider = new ServiceCollection().AddEntityFrameworkInMemoryDatabase().BuildServiceProvider();

            //return new DbContextOptionsBuilder().UseInMemoryDatabase(databaseName).UseInternalServiceProvider(provider).Options;

            return new DbContextOptionsBuilder<MovieDbContext>().UseInMemoryDatabase(databaseName).Options;
        }
    }
}