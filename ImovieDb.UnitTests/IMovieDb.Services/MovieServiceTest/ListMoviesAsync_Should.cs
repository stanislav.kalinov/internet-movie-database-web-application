﻿//using Castle.Core.Configuration;
//using IMovieDb.UnitTests.Common;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using MovieDb.Data.Context;
//using MovieDb.Services;
//using System;
//using System.Linq;
//using System.Threading.Tasks;

//namespace IMovieDb.UnitTests.IMovieDb.Services.MovieServiceTest
//{
//    [TestClass]
//    public class ListMovie_Should
//    {
//        [TestMethod]
//        public async Task AddMovie_Should_Succed()
//        {
//            var options = TestUtils.GetOptions(nameof(AddMovie_Should_Succed));
//            using (var arrangeContext = new MovieDbContext(options))
//            {
//                var sut = new MovieService(arrangeContext);
//                var result = sut.AddMovieAsync("Titanik", new DateTime(2001, 12, 12), 126);
//                await arrangeContext.SaveChangesAsync();
//            }

//            using (var assertContext = new MovieDbContext(options))
//            {
//                var sut = new MovieService(assertContext);
//                Assert.AreEqual(assertContext.Movies.Count(), 1);
//            }
//        }
//    }
//}


//[TestClass]
//public class MovieService_Should
//{
//    [TestMethod]
//    public void AddMovie_Succeed()
//    {
//        var options = TestsUtils.GetOptions(nameof(AddMovie_Succeed));

//        using (var arrangeContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(arrangeContext);
//            var result = sut.AddMovie("Titanik", new DateTime(2001, 12, 12), 126);
//            arrangeContext.SaveChanges();
//        }

//        using (var assertContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(assertContext);
//            Assert.AreEqual(assertContext.Movies.Count(), 1);
//        }
//    }
//    [TestMethod]
//    public void AddMovie_Correct()
//    {
//        var options = TestsUtils.GetOptions(nameof(AddMovie_Correct));

//        using (var arrangeContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(arrangeContext);
//            var result = sut.AddMovie("Titanik", new DateTime(2001, 12, 12), 126);
//            arrangeContext.SaveChanges();
//        }
//        using (var assertContext = new MovieDbContext(options))
//        {
//            Assert.AreEqual(assertContext.Movies.First().Title, "Titanik");
//            Assert.AreEqual(assertContext.Movies.First().ReleaseDate, new DateTime(2001, 12, 12));
//            Assert.AreEqual(assertContext.Movies.First().Duration, 126);
//        }
//    }
//    [TestMethod]
//    public void ThrowException_WhenMovieExist()
//    {
//        var options = TestsUtils.GetOptions(nameof(ThrowException_WhenMovieExist));

//        using (var arrangeContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(arrangeContext);
//            var result = sut.AddMovie("Titanik", new DateTime(2001, 12, 12), 126);
//            arrangeContext.SaveChanges();
//        }
//        using (var assertContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(assertContext);
//            Assert.ThrowsException<ArgumentException>(() => sut.AddMovie("Titanik", new DateTime(2001, 12, 12), 126));
//        }
//    }
//    [TestMethod]
//    public void AddRaiting_Succed()
//    {
//        var options = TestsUtils.GetOptions(nameof(AddRaiting_Succed));

//        using (var arrangeContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(arrangeContext);
//            var result = sut.AddMovie("Titanik", new DateTime(2001, 12, 12), 126);
//            sut.AddRating("Titanik", 8);
//            arrangeContext.SaveChanges();
//        }
//        using (var assertContext = new MovieDbContext(options))
//        {

//            var sut = new MovieService(assertContext);
//            var result = assertContext.Movies.Include(m => m.Rating);
//            Assert.AreEqual(result.Count(), 1);
//        }
//    }
//    [TestMethod]
//    public void AddRating_Correct()
//    {
//        var options = TestsUtils.GetOptions(nameof(AddRating_Correct));

//        using (var assertContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(assertContext);
//            assertContext.Movies.Include(m => m.Rating);
//            var result = sut.AddMovie("Titanik", new DateTime(2001, 12, 12), 126);
//            sut.AddRating("Titanik", 8);
//            var test = result.Rating;
//            assertContext.SaveChanges();

//            Assert.AreEqual(test, 8);
//        }
//    }
//    [TestMethod]
//    public void ChangeRating_Correct()
//    {
//        var options = TestsUtils.GetOptions(nameof(ChangeRating_Correct));

//        using (var assertContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(assertContext);
//            assertContext.Movies.Include(m => m.Rating);
//            var movie = sut.AddMovie("Titanik", new DateTime(2001, 12, 12), 126);
//            sut.AddRating("Titanik", 8);

//            var test = sut.ChangeRating(movie.Title, 10);
//            assertContext.SaveChanges();

//            Assert.AreEqual(assertContext.Movies.First().Rating, 10);

//        }
//    }
//    [TestMethod]
//    public void DeleteMovie_Suceed()
//    {
//        var options = TestsUtils.GetOptions(nameof(DeleteMovie_Suceed));

//        using (var arrangeContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(arrangeContext);
//            var result = sut.AddMovie("Titanik", new DateTime(2001, 12, 12), 126);
//            sut.DeleteMovie(result.Title);
//            arrangeContext.SaveChanges();
//        }

//        using (var assertContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(assertContext);
//            Assert.AreEqual(assertContext.Movies.Count(), 0);
//        }
//    }
//    [TestMethod]
//    public void GetMovieToShow_Should_ReturnCorrectMovie()
//    {
//        var options = TestsUtils.GetOptions(nameof(GetMovieToShow_Should_ReturnCorrectMovie));

//        using (var assertContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(assertContext);
//            var result = sut.AddMovie("Titanik", new DateTime(2001, 12, 12), 126);
//            sut.AddRating(result.Title, 9);
//            var test = sut.GetMovieToShow(result.Title);

//            Assert.AreEqual(result.Title, test.Title);
//            Assert.AreEqual(result.ReleaseDate, test.ReleaseDate);
//            Assert.AreEqual(result.Duration, test.Duration);
//            Assert.AreEqual(result.Rating, test.Rating);
//        }
//    }
//    [TestMethod]
//    public void GetTopRatedMovies_Should_ReturnCorrectMovies()
//    {
//        var options = TestsUtils.GetOptions(nameof(GetTopRatedMovies_Should_ReturnCorrectMovies));

//        using (var arrangeContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(arrangeContext);
//            sut.AddMovie("Titanik", new DateTime(2001, 12, 12), 126);
//            sut.AddMovie("Titanik1", new DateTime(2001, 12, 12), 126);
//            sut.AddMovie("Titanik2", new DateTime(2001, 12, 12), 126);
//            sut.AddMovie("Titanik3", new DateTime(2001, 12, 12), 126);
//            sut.AddMovie("Titanik4", new DateTime(2001, 12, 12), 126);
//            sut.AddRating("Titanik", 8);
//            sut.AddRating("Titanik1", 1);
//            sut.AddRating("Titanik2", 9);
//            sut.AddRating("Titanik3", 2);
//            sut.AddRating("Titanik4", 8);

//            arrangeContext.SaveChanges();
//        }
//        using (var assertContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(assertContext);

//            var result = sut.GetTopRatedMovies();

//            Assert.AreEqual(result.Count, 3);
//        }
//    }
//    [TestMethod]
//    public void GetMoviesByReleaseDate_Should_ReturnCorrectMovies()
//    {
//        var options = TestsUtils.GetOptions(nameof(GetMoviesByReleaseDate_Should_ReturnCorrectMovies));

//        using (var arrangeContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(arrangeContext);

//            sut.AddMovie("Titanik", new DateTime(2001, 12, 12), 126);
//            sut.AddMovie("Titanik1", new DateTime(2002, 12, 12), 126);
//            sut.AddMovie("Titanik2", new DateTime(2001, 10, 12), 126);
//            sut.AddMovie("Titanik3", new DateTime(2005, 12, 12), 126);
//            sut.AddMovie("Titanik4", new DateTime(2006, 12, 12), 126);
//            arrangeContext.SaveChanges();

//        }
//        using (var assertContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(assertContext);
//            var startDate = new DateTime(2001, 12, 12);
//            var endDate = new DateTime(2005, 12, 12);

//            var result = sut.GetMoviesByReleaseDate(startDate, endDate);
//            assertContext.SaveChanges();

//            Assert.AreEqual(result.Count, 3);
//        }
//    }
//    [TestMethod]
//    public void GetMoviesByActor_Should_ReturnSucced()
//    {
//        var options = TestsUtils.GetOptions(nameof(GetMoviesByActor_Should_ReturnSucced));

//        using (var arrangeContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(arrangeContext);

//            var movie = new Movie { Title = "Podprikritie1", ReleaseDate = new DateTime(2001, 12, 12), Duration = 122 };
//            var movie1 = new Movie { Title = "Podprikritie2", ReleaseDate = new DateTime(2001, 12, 12), Duration = 126 };

//            var actor = new Actor { FirstName = "Dimitar", LastName = "Mihov", Birthday = new DateTime(1996, 12, 12) };
//            arrangeContext.Actors.Add(actor);

//            sut.AddMovie("Podprikritie1", new DateTime(2002, 12, 12), 126);
//            sut.AddMovie("Podprikritie2", new DateTime(2001, 10, 12), 126);

//            arrangeContext.MoviesActors.Add(new MoviesActors() { Movie = movie, Actor = actor });
//            arrangeContext.MoviesActors.Add(new MoviesActors() { Movie = movie1, Actor = actor });


//            arrangeContext.SaveChanges();
//        }

//        using (var assertContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(assertContext);

//            var result = sut.GetMoviesByActor("Dimitar", "Mihov");
//            Assert.AreEqual(result.Count, 2);
//        }
//    }
//    [TestMethod]
//    public void GetMoviesByActor_Should_ReturnCorrectMovies()
//    {
//        var options = TestsUtils.GetOptions(nameof(GetMoviesByActor_Should_ReturnCorrectMovies));

//        var movie = new Movie { Title = "Podprikritie1", ReleaseDate = new DateTime(2001, 12, 12), Duration = 122 };
//        var movie1 = new Movie { Title = "Podprikritie2", ReleaseDate = new DateTime(2001, 12, 12), Duration = 126 };

//        using (var arrangeContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(arrangeContext);

//            var actor = new Actor { FirstName = "Dimitar", LastName = "Mihov", Birthday = new DateTime(1996, 12, 12) };
//            arrangeContext.Actors.Add(actor);

//            sut.AddMovie("Podprikritie1", new DateTime(2002, 12, 12), 126);
//            sut.AddMovie("Podprikritie2", new DateTime(2001, 10, 12), 126);

//            arrangeContext.MoviesActors.Add(new MoviesActors() { Movie = movie, Actor = actor });
//            arrangeContext.MoviesActors.Add(new MoviesActors() { Movie = movie1, Actor = actor });


//            arrangeContext.SaveChanges();
//        }

//        using (var assertContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(assertContext);

//            var result = sut.GetMoviesByActor("Dimitar", "Mihov").Select(m => m.Title);
//            Assert.IsTrue(result.Contains(movie.Title));
//            Assert.IsTrue(result.Contains(movie1.Title));

//        }
//    }
//    [TestMethod]
//    public void GetMoviesByDirector_Should_ReturnSucced()
//    {
//        var options = TestsUtils.GetOptions(nameof(GetMoviesByDirector_Should_ReturnSucced));

//        using (var arrangeContext = new MovieDbContext(options))
//        {
//            var director = new Director { FirstName = "Dimitar", LastName = "Mihov" };
//            arrangeContext.Directors.Add(director);

//            var movie = new Movie { Title = "Podprikritie1", ReleaseDate = new DateTime(2001, 12, 12), Duration = 122, Director = director };
//            var movie1 = new Movie { Title = "Podprikritie2", ReleaseDate = new DateTime(2001, 12, 12), Duration = 126, Director = director };

//            arrangeContext.Movies.Add(movie);
//            arrangeContext.Movies.Add(movie1);
//            arrangeContext.SaveChanges();
//        }

//        using (var assertContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(assertContext);

//            var result = sut.GetMoviesByDirector("Dimitar", "Mihov");
//            Assert.AreEqual(result.Count, 2);

//        }
//    }
//    [TestMethod]
//    public void GetMoviesByDirector_Should_ReturnCorrectMovies()
//    {
//        var options = TestsUtils.GetOptions(nameof(GetMoviesByDirector_Should_ReturnCorrectMovies));

//        var director = new Director { FirstName = "Dimitar", LastName = "Mihov" };
//        var movie = new Movie { Title = "Podprikritie1", ReleaseDate = new DateTime(2001, 12, 12), Duration = 122, Director = director };
//        var movie1 = new Movie { Title = "Podprikritie2", ReleaseDate = new DateTime(2001, 12, 12), Duration = 126, Director = director };

//        using (var arrangeContext = new MovieDbContext(options))
//        {
//            arrangeContext.Directors.Add(director);

//            arrangeContext.Movies.Add(movie);
//            arrangeContext.Movies.Add(movie1);
//            arrangeContext.SaveChanges();
//        }

//        using (var assertContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(assertContext);

//            var result = sut.GetMoviesByDirector("Dimitar", "Mihov").Select(m => m.Title).ToList();

//            Assert.IsTrue(result.Contains(movie.Title));
//            Assert.IsTrue(result.Contains(movie1.Title));

//        }
//    }
//    [TestMethod]
//    public void PrintMovie_Should_Correct()
//    {
//        var options = TestsUtils.GetOptions(nameof(PrintMovie_Should_Correct));
//        var movie = new Movie { Title = "Podprikritie1", ReleaseDate = new DateTime(2001, 12, 12), Duration = 122 };

//        using (var arrangeContext = new MovieDbContext(options))
//        {
//            arrangeContext.Movies.Add(movie);
//            arrangeContext.SaveChanges();
//        }
//        using (var assertContext = new MovieDbContext(options))
//        {
//            var sut = new MovieService(assertContext);
//            var result = sut.PrintMovie(movie);
//            Assert.IsTrue(result.Contains(movie.ReleaseDate.ToString("MMMM yyyy")));

//        }
//    }

//}
//}

