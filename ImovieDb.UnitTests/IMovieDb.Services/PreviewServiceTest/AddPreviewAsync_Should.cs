﻿using ImovieDb.Services.Exceptions;
using IMovieDb.UnitTests.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieDb.Data.Context;
using MovieDb.Data.Models;
using MovieDb.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDb.UnitTests.MovieDb.Services.PreviewServiceTest
{
    [TestClass]
    public class AddPreviewAsync_Should
    {
        [TestMethod]
        public async Task Return_PreviewServiceInstance_WhenValidInputIsPassed()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Return_PreviewServiceInstance_WhenValidInputIsPassed")))
            {
                //Arrange
                await arrangeContext.Movies.AddAsync(new Movie()
                {
                    Title = "Little",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Return_PreviewServiceInstance_WhenValidInputIsPassed")))
            {
                //Act
                var sut = new PreviewService(assertContext);

                var preview = await sut.AddPreviewAsync("Little", "Content");

                //Assert
                Assert.IsInstanceOfType(preview, typeof(Preview));
                Assert.AreEqual(preview.Content, "Content");
            }
        }

        [TestMethod]
        public async Task Throw_MovieNotFoundException_WhenMovieDoesNotExist()
        {
            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_MovieNotFoundException_WhenMovieDoesNotExist")))
            {
                //Arrange
                var sut = new PreviewService(assertContext);

                //Act, Assert
                await Assert.ThrowsExceptionAsync<MovieNotFoundException>(async () => await sut.AddPreviewAsync("Little", "Content"));
            }
        }

        [TestMethod]
        public async Task Throw_PreviewAlreadyAssignedException_WhenPreviewIsAlreadyAdded()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Throw_PreviewAlreadyAssignedException_WhenPreviewIsAlreadyAdded")))
            {
                //Arrange
                var movie = new Movie() { Title = "Little" };
                var preview = new Preview() { Content = "Content" };

                await arrangeContext.Movies.AddAsync(movie);

                movie.Preview = preview;
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_PreviewAlreadyAssignedException_WhenPreviewIsAlreadyAdded")))
            {
                //Act
                var sut = new PreviewService(assertContext);

                //Assert
                await Assert.ThrowsExceptionAsync<PreviewAlreadyAssignedException>(async () => await sut.AddPreviewAsync("Little", "Content"));
            }
        }

        [TestMethod]
        public async Task Add_Preview_IfDoesNotExist()
        {
            //Arrange
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Add_Preview_IfDoesNotExist")))
            {
                await arrangeContext.Movies.AddAsync(new Movie() { Title = "Little" });

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Add_Preview_IfDoesNotExist")))
            {
                //Act
                var sut = new PreviewService(assertContext);

                var preview = await sut.AddPreviewAsync("Little", "Content");

                var movie = await assertContext.Movies.FirstOrDefaultAsync(m => m.Title == "Little");

                //Assert
                Assert.IsTrue(assertContext.Previews.First() == preview);
                Assert.IsTrue(assertContext.Previews.Count() == 1);

                Assert.IsTrue(movie.Preview == preview);
            }
        }
    }
}
