﻿using IMovieDb.UnitTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieDb.Data.Context;
using MovieDb.Services;
using System;

namespace MovieDb.UnitTests.MovieDb.Services.PreviewServiceTest
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void Create_PreviewServiceInstance_WhenValidInputIsPassed()
        {
            using (var context = new MovieDbContext(TestUtils.GetOptions("Create_PreviewServiceInstance_WhenValidInputIsPassed")))
            {
                //Arrange, Act
                var sut = new PreviewService(context);

                //Assert
                Assert.IsInstanceOfType(sut, typeof(PreviewService));
            }
        }

        [TestMethod]
        public void Throw_ArgumentNullException_WhenNullArgumentIsPassed()
        {
            using (var context = new MovieDbContext(TestUtils.GetOptions("Throw_ArgumentNullException_WhenNullArgumentIsPassed")))
            {
                //Arrange, Act, Assert
                Assert.ThrowsException<ArgumentNullException>(() => new PreviewService(null));
            }
        }
    }
}
