﻿using ImovieDb.Services.Exceptions;
using IMovieDb.UnitTests.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieDb.Data.Context;
using MovieDb.Data.Models;
using MovieDb.Services;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDb.UnitTests.MovieDb.Services.PreviewServiceTest
{
    [TestClass]
    public class ChangePreviewAsync_Should
    {
        [TestMethod]
        public async Task Return_NewPreviewServiceInstance_WhenValidInputIsPassed()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Return_NewPreviewServiceInstance_WhenValidInputIsPassed")))
            {
                //Arrange
                var movie = new Movie() { Title = "Little" };
                var preview = new Preview() { Content = "Content" };

                await arrangeContext.Movies.AddAsync(movie);
                movie.Preview = preview;

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Return_NewPreviewServiceInstance_WhenValidInputIsPassed")))
            {
                //Act
                var sut = new PreviewService(assertContext);

                var preview = await sut.ChangePreviewAsync("Little", "NewContent");

                //Assert
                Assert.IsInstanceOfType(preview, typeof(Preview));
                Assert.AreEqual(preview.Content, "NewContent");
            }
        }

        [TestMethod]
        public async Task Throw_MovieNotFoundException_WhenMovieDoesNotExist()
        {
            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_MovieNotFoundException_WhenMovieDoesNotExist")))
            {
                //Arrange
                var sut = new PreviewService(assertContext);

                //Act, Assert
                await Assert.ThrowsExceptionAsync<MovieNotFoundException>(async () => await sut.AddPreviewAsync("Little", "Content"));
            }
        }

        [TestMethod]
        public async Task Throw_PreviewNotFoundException_WhenPreviewIsNotAdded()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Throw_PreviewNotFoundException_WhenPreviewIsNotAdded")))
            {
                //Arrange
                var movie = new Movie() { Title = "Little" };

                await arrangeContext.Movies.AddAsync(movie);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_PreviewNotFoundException_WhenPreviewIsNotAdded")))
            {
                //Act
                var sut = new PreviewService(assertContext);

                //Assert
                await Assert.ThrowsExceptionAsync<PreviewNotFoundException>(async () => await sut.ChangePreviewAsync("Little", "Content"));
            }
        }

        [TestMethod]
        public async Task Change_Preview_IfExist()
        {
            //Arrange
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Change_Preview_IfExist")))
            {
                var movie = new Movie() { Title = "Little" };
                var preview = new Preview() { Content = "Content" };

                await arrangeContext.Movies.AddAsync(movie);
                movie.Preview = preview;

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Change_Preview_IfExist")))
            {
                //Act
                var sut = new PreviewService(assertContext);

                var movie = await assertContext.Movies.FirstOrDefaultAsync(m => m.Title == "Little");

                var oldPreview = movie.Preview; 

                var newPreview = await sut.ChangePreviewAsync("Little", "NewContent");

                //Assert
                Assert.IsTrue(assertContext.Previews.First() == newPreview);
                Assert.IsTrue(assertContext.Previews.First() != oldPreview);
                Assert.IsTrue(assertContext.Previews.Count() == 1);

                Assert.IsTrue(movie.Preview == newPreview);
                Assert.IsTrue(movie.Preview != oldPreview);
            }
        }
    }
}
