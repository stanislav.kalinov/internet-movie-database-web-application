﻿using ImovieDb.Services.Exceptions;
using IMovieDb.UnitTests.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieDb.Data.Context;
using MovieDb.Data.Models;
using MovieDb.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDb.UnitTests.MovieDb.Services.GenreServiceTest
{
    [TestClass]
    public class DeleteGenreAsync_Should
    {
        [TestMethod]
        public async Task Delete_Genre_WhenValidInputIsPassed()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Delete_Genre_WhenValidInputIsPassed")))
            {
                //Arrange
                var movie = new Movie() { Title = "Little" };
                var mainGenre = new Genre() { Type = "Feature film" };
                var genre = new Genre() { Type = "Action" };

                await arrangeContext.Movies.AddAsync(movie);
                movie.MainGenre = mainGenre;
                await arrangeContext.Genres.AddAsync(mainGenre);
                await arrangeContext.Genres.AddAsync(genre);
                await arrangeContext.MoviesGenres.AddAsync(new MoviesGenres() { Movie = movie, Genre = genre });

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Delete_Genre_WhenValidInputIsPassed")))
            {
                //Act
                var sut = new GenreService(assertContext);

                await sut.DeleteGenreAsync("Little", "Action");

                var movie = await assertContext.Movies.FirstOrDefaultAsync(m => m.Title == "Little");

                //Assert
                Assert.IsTrue(assertContext.MoviesGenres.Count() == 0);
                Assert.IsTrue(movie.MainGenre == null);
            }
        }

        [TestMethod]
        public async Task Throw_MovieNotFoundException_WhenMovieDoesNotExist()
        {
            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_MovieNotFoundException_WhenMovieDoesNotExist")))
            {
                //Arrange
                var sut = new GenreService(assertContext);

                //Act, Assert
                await Assert.ThrowsExceptionAsync<MovieNotFoundException>(async () => await sut.DeleteGenreAsync("Little", "Action"));
            }
        }

        [TestMethod]
        public async Task Throw_GenreNotFoundException_WhenNotAssignedGenreIsPassed()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Throw_GenreNotFoundException_WhenNotAssignedGenreIsPassed")))
            {
                //Arrange
                var movie = new Movie() { Title = "Little" };
                var mainGenre = new Genre() { Type = "Feature film" };
                var genre = new Genre() { Type = "Action" };

                await arrangeContext.Movies.AddAsync(movie);
                movie.MainGenre = mainGenre;
                await arrangeContext.Genres.AddAsync(mainGenre);
                await arrangeContext.Genres.AddAsync(genre);
                await arrangeContext.MoviesGenres.AddAsync(new MoviesGenres() { Movie = movie, Genre = genre });

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_GenreNotFoundException_WhenNotAssignedGenreIsPassed")))
            {
                //Arrange
                var sut = new GenreService(assertContext);

                //Act, Assert
                await Assert.ThrowsExceptionAsync<GenreNotFoundException>(async () => await sut.DeleteGenreAsync("Little", "Comedy"));
            }
        }
    }
}
