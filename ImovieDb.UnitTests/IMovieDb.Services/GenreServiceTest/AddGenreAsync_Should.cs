﻿using ImovieDb.Services.Exceptions;
using IMovieDb.UnitTests.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieDb.Data.Context;
using MovieDb.Data.Models;
using MovieDb.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDb.UnitTests.MovieDb.Services.GenreServiceTest
{
    [TestClass]
    public class AddGenreAsync_Should
    {
        [TestMethod]
        public async Task Return_Genre_WhenValidInputIsPassed()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Return_Genre_WhenValidInputIsPassed")))
            {
                //Arrange
                var movie = new Movie() { Title = "Little" };
                var mainGenre = new Genre() { Type = "Feature film"};

                await arrangeContext.Movies.AddAsync(movie);
                await arrangeContext.Genres.AddAsync(mainGenre);
                movie.MainGenre = mainGenre;

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Return_Genre_WhenValidInputIsPassed")))
            {
                //Act
                var sut = new GenreService(assertContext);

                var genre = await sut.AddGenreAsync("Little", "Action");

                //Assert
                Assert.IsInstanceOfType(genre, typeof(Genre));
                Assert.AreEqual(genre.Type, "Action");
            }
        }

        [TestMethod]
        public async Task Throw_MovieNotFoundException_WhenMovieDoesNotExist()
        {
            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_MovieNotFoundException_WhenMovieDoesNotExist")))
            {
                //Arrange
                var sut = new GenreService(assertContext);

                //Act, Assert
                await Assert.ThrowsExceptionAsync<MovieNotFoundException>(async () => await sut.AddGenreAsync("Little", "Action"));
            }
        }

        [TestMethod]
        public async Task Throw_GenreAlreadyAssignedException_WhenGenreIsAlreadyAssigned()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Throw_GenreAlreadyAssignedException_WhenGenreIsAlreadyAssigned")))
            {
                //Arrange
                var movie = new Movie() { Title = "Little" };
                var mainGenre = new Genre() { Type = "Feature film" };
                var genre = new Genre() { Type = "Action" };

                await arrangeContext.Movies.AddAsync(movie);
                movie.MainGenre = mainGenre;
                await arrangeContext.Genres.AddAsync(mainGenre);
                await arrangeContext.Genres.AddAsync(genre);
                await arrangeContext.MoviesGenres.AddAsync(new MoviesGenres() { Movie = movie, Genre = genre });

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_GenreAlreadyAssignedException_WhenGenreIsAlreadyAssigned")))
            {
                //Act
                var sut = new GenreService(assertContext);

                //Assert
                await Assert.ThrowsExceptionAsync<GenreAlreadyAssignedException>(async () => await sut.AddGenreAsync("Little", "Action"));
            }
        }

        [TestMethod]
        public async Task Throw_MainGenreNotFoundException_WhenInvalidMainGenreIsPassed()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Throw_MainGenreNotFoundException_WhenInvalidMainGenreIsPassed")))
            {
                //Arrange
                var movie = new Movie() { Title = "Little" };
                var mainGenre = new Genre() { Type = "Feat" };

                await arrangeContext.Movies.AddAsync(movie);
                movie.MainGenre = mainGenre;
                await arrangeContext.Genres.AddAsync(mainGenre);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_MainGenreNotFoundException_WhenInvalidMainGenreIsPassed")))
            {
                //Act
                var sut = new GenreService(assertContext);

                //Assert
                await Assert.ThrowsExceptionAsync<MainGenreNotFoundException>(async () => await sut.AddGenreAsync("Little", "Action"));
            }
        }

        [TestMethod]
        public async Task Throw_MainGenreNotFoundException_WhenNotCorrespondingGenreIsPassed()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Throw_MainGenreNotFoundException_WhenNotCorrespondingGenreIsPassed")))
            {
                //Arrange
                var movie = new Movie() { Title = "Little" };
                var mainGenre = new Genre() { Type = "Feature film" };

                await arrangeContext.Movies.AddAsync(movie);
                movie.MainGenre = mainGenre;
                await arrangeContext.Genres.AddAsync(mainGenre);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_MainGenreNotFoundException_WhenNotCorrespondingGenreIsPassed")))
            {
                //Act
                var sut = new GenreService(assertContext);

                //Assert
                await Assert.ThrowsExceptionAsync <MainGenreNotFoundException>(async () => await sut.AddGenreAsync("Little", "Space"));
            }
        }

        [TestMethod]
        public async Task Add_Genre_WhenValidInputIsPassed()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Add_Genre_WhenValidInputIsPassed")))
            {
                //Arrange
                var movie = new Movie() { Title = "Little" };
                var mainGenre = new Genre() { Type = "Feature film" };

                await arrangeContext.Movies.AddAsync(movie);
                await arrangeContext.Genres.AddAsync(mainGenre);
                movie.MainGenre = mainGenre;

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Add_Genre_WhenValidInputIsPassed")))
            {
                //Act
                var sut = new GenreService(assertContext);

                var genre = await sut.AddGenreAsync("Little", "Action");

                var movie = await assertContext.Movies.FirstOrDefaultAsync(m => m.Title == "Little");

                //Assert
                Assert.IsTrue(assertContext.Genres.Contains(movie.MainGenre));
                Assert.IsTrue(assertContext.Genres.Contains(genre));
                Assert.IsTrue(assertContext.Genres.Count() == 2);

                Assert.IsTrue(assertContext.MoviesGenres.Select(ma => ma.Genre).First() == genre);
                Assert.IsTrue(assertContext.MoviesGenres.Count() == 1);

                Assert.IsTrue(movie.MainGenre.Type == "Feature film");
            }
        }
    }
}
