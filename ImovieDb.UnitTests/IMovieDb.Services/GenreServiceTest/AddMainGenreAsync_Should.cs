﻿using ImovieDb.Services.Exceptions;
using IMovieDb.UnitTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieDb.Data.Context;
using MovieDb.Data.Models;
using MovieDb.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDb.UnitTests.MovieDb.Services.GenreServiceTest
{
    [TestClass]
    public class AddMainGenreAsync_Should
    {
        [TestMethod]
        public async Task Return_MainGenre_WhenValidInputIsPassed()
        {
            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Return_MainGenre_WhenValidInputIsPassed")))
            {
                //Arrange, Act
                var sut = new GenreService(assertContext);

                var genre = await sut.AddMainGenreAsync("Feature film");

                //Assert
                Assert.IsInstanceOfType(genre, typeof(Genre));
                Assert.AreEqual(genre.Type, "Feature film");
            }
        }

        [TestMethod]
        public async Task Throw_MovieDbArgumentException_WhenInvalidGenreTypeIsPassed()
        {
            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_MovieDbArgumentException_WhenInvalidGenreTypeIsPassed")))
            {
                //Arrange, Act
                var sut = new GenreService(assertContext);

                //Assert
                await Assert.ThrowsExceptionAsync<MovieDbArgumentException>(async () => await sut.AddMainGenreAsync("Feat"));
            }
        }

        [TestMethod]
        public async Task Throw_MainGenreAlreadyExistsException_WhenGenreTypeIsAlreadyAdded()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Throw_MainGenreAlreadyExistsException_WhenGenreTypeIsAlreadyAdded")))
            {
                //Arrange
                await arrangeContext.Genres.AddAsync(new Genre { Type = "Feature film" });

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_MainGenreAlreadyExistsException_WhenGenreTypeIsAlreadyAdded")))
            {
                //Act
                var sut = new GenreService(assertContext);

                //Assert
                await Assert.ThrowsExceptionAsync<MainGenreAlreadyExistsException>(async () => await sut.AddMainGenreAsync("Feature film"));
            }
        }

        [TestMethod]
        public async Task Add_MainGenre_WhenValidInputIsPassed()
        {
            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Add_MainGenre_WhenValidInputIsPassed")))
            {
                //Act
                var sut = new GenreService(assertContext);

                var mainGenre = await sut.AddMainGenreAsync("Feature film");

                //Assert
                Assert.IsTrue(assertContext.Genres.Contains(mainGenre));
                Assert.IsTrue(assertContext.Genres.Count() == 1);
                Assert.IsTrue(mainGenre.Type == "Feature film");
            }
        }
    }
}
