﻿using IMovieDb.UnitTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieDb.Data.Context;
using MovieDb.Services;
using System;

namespace MovieDb.UnitTests.MovieDb.Services.GenreServiceTest
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void Create_GenreServiceInstance_WhenValidInputIsPassed()
        {
            using (var context = new MovieDbContext(TestUtils.GetOptions("Create_GenreServiceInstance_WhenValidInputIsPassed")))
            {
                //Arrange, Act
                var sut = new GenreService(context);

                //Assert
                Assert.IsInstanceOfType(sut, typeof(GenreService));
            }
        }

        [TestMethod]
        public void Throw_ArgumentNullException_WhenNullArgumentIsPassed()
        {
            using (var context = new MovieDbContext(TestUtils.GetOptions("Throw_ArgumentNullException_WhenNullContextIsPassed")))
            {
                //Arrange, Act, Assert
                Assert.ThrowsException<ArgumentNullException>(() => new GenreService(null));
            }
        }
    }
}
