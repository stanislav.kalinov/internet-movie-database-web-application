﻿using ImovieDb.Services.Exceptions;
using IMovieDb.UnitTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieDb.Data.Context;
using MovieDb.Data.Models;
using MovieDb.Services;
using System;
using System.Threading.Tasks;

namespace MovieDb.UnitTests.MovieDb.Services.GenreServiceTest
{
    [TestClass]
    public class GetMainGenreAsync_Should
    {
        [TestMethod]
        public async Task Return_MainGenre_WhenValidInputIsPassed()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Return_MainGenre_WhenValidInputIsPassed")))
            {
                var mainGenre = new Genre() { Type = "Feature film" };

                await arrangeContext.Genres.AddAsync(mainGenre);

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Return_MainGenre_WhenValidInputIsPassed")))
            {
                //Act
                var sut = new GenreService(assertContext);

                var genre = await sut.GetMainGenreAsync("Action");

                //Assert
                Assert.IsInstanceOfType(genre, typeof(Genre));
                Assert.AreEqual(genre.Type, "Feature film");
            }
        }

        [TestMethod]
        public async Task Throw_MovieDbArgumentException_WhenInvalidGenreIsPassed()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Throw_MovieDbArgumentException_WhenInvalidGenreIsPassed")))
            {
                var mainGenre = new Genre() { Type = "Feature film" };

                await arrangeContext.Genres.AddAsync(mainGenre);

                await arrangeContext.SaveChangesAsync();
            }


            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_MovieDbArgumentException_WhenInvalidGenreIsPassed")))
            {
                //Arrange
                var sut = new GenreService(assertContext);

                //Act, Assert
                await Assert.ThrowsExceptionAsync<MovieDbArgumentException>(async () => await sut.GetMainGenreAsync("Act"));
            }
        }
    }
}
