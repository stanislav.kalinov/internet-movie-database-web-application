﻿using ImovieDb.Services.Exceptions;
using IMovieDb.UnitTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieDb.Data.Context;
using MovieDb.Data.Models;
using MovieDb.Services;
using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDb.UnitTests.MovieDb.Services.ActorServiceTest
{
    [TestClass]
    public class AddActorAsync_Should
    {
        [TestMethod]
        public async Task Return_Actor_WhenValidInputIsPassed()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Return_Actor_WhenValidInputIsPassed")))
            {
                //Arrange
                await arrangeContext.Movies.AddAsync(new Movie()
                {
                    Title = "Little",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Return_Actor_WhenValidInputIsPassed")))
            {
                //Act
                var sut = new ActorService(assertContext);

                var actor = await sut.AddActorAsync("Little", "Pesho", "Peshev", DateTime.ParseExact("13/12/2006", "dd/MM/yyyy",
                                                CultureInfo.CreateSpecificCulture("de-De")));

                //Assert
                Assert.IsInstanceOfType(actor, typeof(Actor));
                Assert.AreEqual(actor.FirstName, "Pesho");
                Assert.AreEqual(actor.LastName, "Peshev");
                Assert.AreEqual(actor.Birthday, DateTime.ParseExact("13/12/2006", "dd/MM/yyyy",
                                                CultureInfo.CreateSpecificCulture("de-De")));
            }
        }

        [TestMethod]
        public async Task Throw_MovieNotFoundException_WhenMovieDoesNotExist()
        {
            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_MovieNotFoundException_WhenMovieDoesNotExist")))
            {
                //Arrange
                var sut = new ActorService(assertContext);

                //Act, Assert
                await Assert.ThrowsExceptionAsync<MovieNotFoundException>(async () => await sut.AddActorAsync("Little", "Pesho", "Peshev", null));
            }
        }

        [TestMethod]
        public async Task Throw_ActorAlreadyAssignedException_WhenActorIsAlreadyAssigned()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Throw_ActorAlreadyAssignedException_WhenActorIsAlreadyAssigned")))
            {
                //Arrange
                var movie = new Movie() { Title = "Little" };
                var actor = new Actor() { FirstName = "Pesho", LastName = "Peshev" };


                await arrangeContext.Movies.AddAsync(movie);

                await arrangeContext.MoviesActors.AddAsync(new MoviesActors() { Movie = movie, Actor = actor });
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_ActorAlreadyAssignedException_WhenActorIsAlreadyAssigned")))
            {
                //Act
                var sut = new ActorService(assertContext);

                //Assert
                await Assert.ThrowsExceptionAsync<ActorAlreadyAssignedException>(async () => await sut.AddActorAsync("Little", "Pesho", "Peshev", DateTime.ParseExact("13/12/2006", "dd/MM/yyyy",
                                                CultureInfo.CreateSpecificCulture("de-De"))));
            }
        }

        [TestMethod]
        public async Task Add_Actor_IfDoesNotExist()
        {
            //Arrange
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Add_Actor_IfDoesNotExist")))
            {
                await arrangeContext.Movies.AddAsync(new Movie() { Title = "Little" });

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Add_Actor_IfDoesNotExist")))
            {
                //Act
                var sut = new ActorService(assertContext);

                var actor = await sut.AddActorAsync("Little", "Pesho", "Peshev", DateTime.ParseExact("13/12/2006", "dd/MM/yyyy",
                                                CultureInfo.CreateSpecificCulture("de-De")));

                //Assert
                Assert.IsTrue(assertContext.Actors.First() == actor);
                Assert.IsTrue(assertContext.Actors.Count() == 1);

                Assert.IsTrue(assertContext.MoviesActors.Select(ma => ma.Actor).First() == actor);
                Assert.IsTrue(assertContext.MoviesActors.Count() == 1);
            }
        }
    }
}
