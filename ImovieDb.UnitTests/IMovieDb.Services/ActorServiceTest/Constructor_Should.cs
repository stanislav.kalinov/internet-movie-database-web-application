﻿using IMovieDb.UnitTests.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieDb.Data.Context;
using MovieDb.Services;
using System;
using System.Threading.Tasks;

namespace MovieDb.UnitTests.MovieDb.Services.ActorServiceTest
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void Create_ActorServiceInstance_WhenValidInputIsPassed()
        {
            using (var context = new MovieDbContext(TestUtils.GetOptions("Create_ActorServiceInstance_WhenValidInputIsPassed")))
            {
                //Arrange, Act
                var sut = new ActorService(context);

                //Assert
                Assert.IsInstanceOfType(sut, typeof(ActorService));
            }
        }

        [TestMethod]
        public void Throw_ArgumentNullException_WhenNullArgumentIsPassed()
        {
            using (var context = new MovieDbContext(TestUtils.GetOptions("Throw_ArgumentNullException_WhenNullContextIsPassed")))
            {
                //Arrange, Act, Assert
                Assert.ThrowsException<ArgumentNullException>(() => new ActorService(null));
            }
        }
    }
}
