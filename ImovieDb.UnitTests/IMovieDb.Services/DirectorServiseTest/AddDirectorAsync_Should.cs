﻿using ImovieDb.Services.Exceptions;
using IMovieDb.UnitTests.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MovieDb.Data.Context;
using MovieDb.Data.Models;
using MovieDb.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MovieDb.UnitTests.MovieDb.Services.DirectorServiseTest
{
    [TestClass]
    public class AddDirectorAsync_Should
    {
        [TestMethod]
        public async Task Return_DirectorServiceInstance_WhenValidInputIsPassed()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Return_DirectorServiceInstance_WhenValidInputIsPassed")))
            {
                //Arrange
                await arrangeContext.Movies.AddAsync(new Movie()
                {
                    Title = "Little",
                });

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Return_DirectorServiceInstance_WhenValidInputIsPassed")))
            {
                //Act
                var sut = new DirectorService(assertContext);

                var director = await sut.AddDirectorAsync("Little", "Pesho", "Peshev");

                //Assert
                Assert.IsInstanceOfType(director, typeof(Director));
                Assert.AreEqual(director.FirstName, "Pesho");
                Assert.AreEqual(director.LastName, "Peshev");
            }
        }

        [TestMethod]
        public async Task Throw_MovieNotFoundException_WhenMovieDoesNotExist()
        {
            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_MovieNotFoundException_WhenMovieDoesNotExist")))
            {
                //Arrange
                var sut = new DirectorService(assertContext);

                //Act, Assert
                await Assert.ThrowsExceptionAsync<MovieNotFoundException>(async () => await sut.AddDirectorAsync("Little", "Pesho", "Peshev"));
            }
        }

        [TestMethod]
        public async Task Throw_DirectorAlreadyAssignedException_WhenDirectorIsAlreadyAssigned()
        {
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Throw_DirectorAlreadyAssignedException_WhenDirectorIsAlreadyAssigned")))
            {
                //Arrange
                var movie = new Movie() { Title = "Little" };
                var director = new Director() { FirstName = "Pesho", LastName = "Peshev" };

                await arrangeContext.Movies.AddAsync(movie);

                movie.Director = director;
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Throw_DirectorAlreadyAssignedException_WhenDirectorIsAlreadyAssigned")))
            {
                //Act
                var sut = new DirectorService(assertContext);

                //Assert
                await Assert.ThrowsExceptionAsync<DirectorAlreadyAssignedException>(async () => await  sut.AddDirectorAsync("Little", "Pesho", "Peshev"));
            }
        }

        [TestMethod]
        public async Task Add_Director_IfDoesNotExist()
        {
            //Arrange
            using (var arrangeContext = new MovieDbContext(TestUtils.GetOptions("Add_Director_IfDoesNotExist")))
            {
                await arrangeContext.Movies.AddAsync(new Movie() { Title = "Little" });

                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new MovieDbContext(TestUtils.GetOptions("Add_Director_IfDoesNotExist")))
            {
                //Act
                var sut = new DirectorService(assertContext);

                var director = await sut.AddDirectorAsync("Little", "Pesho", "Peshev");

                var movie = await assertContext.Movies.FirstOrDefaultAsync(m => m.Title == "Little");

                //Assert
                Assert.IsTrue(assertContext.Directors.First() == director);
                Assert.IsTrue(assertContext.Directors.Count() == 1);

                Assert.IsTrue(movie.Director == director);
            }
        }
    }
}
