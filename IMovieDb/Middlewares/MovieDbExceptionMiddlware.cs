﻿using ImovieDb.Services.Exceptions;
using Microsoft.AspNetCore.Http;
using System;
using System.Net;
using System.Threading.Tasks;

namespace IMovieDb.Middlewares
{
    public class MovieDbExceptionMiddleware
    {
        private readonly RequestDelegate next;

        public MovieDbExceptionMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await next.Invoke(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var response = context.Response;
            var customException = exception as BaseIMovieDbException;
            var statusCode = (int) HttpStatusCode.InternalServerError;
            var message = "Unexpected error occured!";

            if (customException != null)
            {
                message = customException.Message;
                statusCode = customException.Code;
            }

            //response.ContentType = "application/json";
            //await response.WriteAsync(JsonConvert.SerializeObject(message));

            response.ContentType = "text/html";
            response.StatusCode = statusCode;
            await response.WriteAsync("<html lang=\"en\"><body>\r\n");
            await response.WriteAsync("<br><h2 style=\"color: red;\"><strong>ERROR!</strong></h2>");
            await response.WriteAsync($"<p>{message}<p><br>");
            await response.WriteAsync("<a href=\"/\"><button class=\"button button5\">Home</button></a><br>\r\n");
            await response.WriteAsync("</body></html>\r\n");
            await response.WriteAsync(new string(' ', 512));
        }
    }
}
