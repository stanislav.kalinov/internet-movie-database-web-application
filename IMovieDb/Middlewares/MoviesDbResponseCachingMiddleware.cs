﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.ResponseCaching;
using System;
using System.Threading.Tasks;

namespace IMovieDb.Middlewares
{
    public class MoviesDbResponseCachingMiddleware
    {
        private readonly RequestDelegate _next;

        public MoviesDbResponseCachingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            context.Response.GetTypedHeaders().CacheControl =
            new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
            {
                Public = true,
                MaxAge = TimeSpan.FromSeconds(30)
            };

            var responseCachingFeature = context.Features.Get<IResponseCachingFeature>();

            if (responseCachingFeature != null)
            {
                responseCachingFeature.VaryByQueryKeys = new[] { "*" };
            }
            await _next(context);
        }
    }
}
