﻿using Core_2._2.Mappers;
using Core_2._2.Mappers.Contracts;
using Core_2._2.Models;
using IMovieDb.Data.Models;
using IMovieDb.Mappers;
using IMovieDb.Middlewares;
using IMovieDb.Models;
using IMovieDb.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MovieDb.Data.Context;
using MovieDb.Data.Models;
using MovieDb.Services;
using MovieDb.Services.Contracts;
using System.Collections.Generic;
using System.IO;

namespace IMovieDb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<MovieDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<MovieDbContext>()
                .AddDefaultTokenProviders();

            services.AddScoped<IActorService, ActorService>();
            services.AddScoped<IDirectorService, DirectorService>();
            services.AddScoped<IGenreService, GenreService>();
            services.AddScoped<IMovieService, MovieService>();
            services.AddScoped<IPreviewService, PreviewService>();

            services.AddSingleton<IViewModelMapper<Movie, MovieViewModel>, MovieViewModelMapper>();
            services.AddSingleton<IViewModelMapper<Director, DirectorViewModel>, DirectorViewModelMapper>();
            services.AddSingleton<IViewModelMapper<Actor, ActorViewModel>, ActorViewModelMapper>();
            services.AddSingleton<IViewModelMapper<Preview, PreviewViewModel>, PreviewViewModelMapper>();
            services.AddSingleton<IViewModelMapper<Movie, RatingViewModel>, RatingViewModelMapper>();
            services.AddSingleton<IViewModelMapper<Movie, GenresViewModel>, GenresViewModelMapper>();
            services.AddSingleton<IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>, MoviesViewModelMapper>();
            services.AddSingleton<IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>, SearchViewModelMapper>();

            services.AddTransient<IEmailSender, EmailSender>();

            services.AddScoped<MovieViewModel>();
            services.AddScoped<MoviesViewModel>();
            services.AddScoped<DirectorViewModel>();
            services.AddScoped<ActorViewModel>();
            services.AddScoped<ActorsViewModel>();
            services.AddScoped<PreviewViewModel>();
            services.AddScoped<RatingViewModel>();
            services.AddScoped<GenresViewModel>();
            services.AddScoped<MainGenresViewModel>();
            services.AddScoped<DeleteMovieViewModel>();
            services.AddScoped<ListMovieByMainGenreViewModel>();
            services.AddScoped<ListMovieByDirectorViewModel>();
            services.AddScoped<ListMovieByMainGenreViewModel>();
            services.AddScoped<ListMovieByGenreViewModel>();
            services.AddScoped<SearchViewModel>();
            services.AddScoped<ReviewViewModel>();
            services.AddScoped<UserRatingViewModel>();

            services.AddResponseCaching();

            services.AddMvc().SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_2_2);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseMiddleware<MovieDbExceptionMiddleware>();
                app.UseHsts();
            }

            app.UseStatusCodePages(async context =>
            {
                context.HttpContext.Response.ContentType = "text/plain";

                await context.HttpContext.Response.WriteAsync(
                    "Status code page, status code: " +
                    context.HttpContext.Response.StatusCode);
            });

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMiddleware<MoviesDbResponseCachingMiddleware>();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
