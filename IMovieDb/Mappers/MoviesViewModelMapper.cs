﻿using Core_2._2.Mappers.Contracts;
using Core_2._2.Models;
using MovieDb.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core_2._2.Mappers
{
    public class MoviesViewModelMapper : IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel>
    {
        private readonly IViewModelMapper<Movie, MovieViewModel> movieMapper;

        public MoviesViewModelMapper(IViewModelMapper<Movie, MovieViewModel> movieMapper)
        {
            this.movieMapper = movieMapper ?? throw new ArgumentNullException(nameof(movieMapper));
        } 

        public MoviesViewModel MapFrom(IReadOnlyCollection<Movie> movies)
        {
            var moviesViewModel = new MoviesViewModel
            {
                Movies = movies.Select(this.movieMapper.MapFrom).ToList()
            };

            return moviesViewModel;
        }
    }
}
