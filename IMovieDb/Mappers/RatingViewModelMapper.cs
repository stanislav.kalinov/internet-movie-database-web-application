﻿using Core_2._2.Mappers.Contracts;
using Core_2._2.Models;
using MovieDb.Data.Models;

namespace IMovieDb.Mappers
{
    public class RatingViewModelMapper : IViewModelMapper<Movie, RatingViewModel>
    {
        public RatingViewModel MapFrom(Movie entity)
        {
            var ratingViewModel = new RatingViewModel
            {
                Rating = (int) entity.Rating,
                MovieTitle = entity.Title
            };

            return ratingViewModel;
        }
    }
}
