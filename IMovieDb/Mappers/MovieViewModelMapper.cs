﻿using Core_2._2.Mappers.Contracts;
using Core_2._2.Models;
using MovieDb.Data.Models;
using System.Linq;

namespace Core_2._2.Mappers
{
    public class MovieViewModelMapper : IViewModelMapper<Movie, MovieViewModel>
    {
        public MovieViewModel MapFrom(Movie entity)
        {
            var movieViewModel = new MovieViewModel
            {
                Id = entity.Id,
                Title = entity.Title,
                ReleaseDate = entity.ReleaseDate,
                Duration = entity.Duration,
                ImageURL = entity.ImageURL,
                Rating = entity.Rating,
                UserRating = entity.UserRating,
                Voters = entity.Voters,
                Director = entity.Director,
                Preview = entity.Preview,
                MainGenre = entity.MainGenre,
                Reviews = entity.Reviews,
                Actors = entity.MoviesActors.Select(ma => ma.Actor).ToList(),
                MoviesGenres = entity.MoviesGenres.Select(mg=>mg.Genre).ToList(),
                MovieGenres = entity.MoviesGenres.Select(mg => mg.Genre.Type).ToList(),
            };

            return movieViewModel;
        }
    }
}
