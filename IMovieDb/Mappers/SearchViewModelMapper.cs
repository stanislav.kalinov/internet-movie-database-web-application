﻿using Core_2._2.Mappers.Contracts;
using Core_2._2.Models;
using MovieDb.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace IMovieDb.Mappers
{
    public class SearchViewModelMapper : IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel>
    {
        private readonly IViewModelMapper<Movie, MovieViewModel> movieMapper;

        public SearchViewModelMapper(IViewModelMapper<Movie, MovieViewModel> movieMapper)
        {
            this.movieMapper = movieMapper;
        }

        public SearchViewModel MapFrom(IReadOnlyCollection<Movie> entity)
        {
            var searchViewModel = new SearchViewModel
            {
                Movies = entity.Select(e => this.movieMapper.MapFrom(e)).ToList(),
            };

            return searchViewModel;
        }
    }
}
