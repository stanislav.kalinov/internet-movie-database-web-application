﻿using Core_2._2.Mappers.Contracts;
using IMovieDb.Data.Models;
using MovieDb.Data.Models;

namespace IMovieDb.Mappers
{
    public class DirectorViewModelMapper : IViewModelMapper<Director, DirectorViewModel>
    {
        public DirectorViewModel MapFrom(Director entity)
        {
            var directorViewModel = new DirectorViewModel
            {
                Id = entity.Id,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Movies = entity.Movies
            };

            return directorViewModel;
        }
    }
}
