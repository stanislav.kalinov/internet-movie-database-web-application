﻿using Core_2._2.Mappers.Contracts;
using Core_2._2.Models;
using MovieDb.Data.Models;
using System.Linq;

namespace IMovieDb.Mappers
{
    public class GenresViewModelMapper : IViewModelMapper<Movie, GenresViewModel>
    {
        public GenresViewModel MapFrom(Movie entity)
        {
            var genresViewModel = new GenresViewModel
            {
               Genres  = entity.MoviesGenres.Select(mg => mg.Genre.Type).ToList(),
                MovieTitle = entity.Title
            };

            return genresViewModel;
        }
    }
}
