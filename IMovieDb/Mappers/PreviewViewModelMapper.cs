﻿using Core_2._2.Mappers.Contracts;
using Core_2._2.Models;
using MovieDb.Data.Models;

namespace IMovieDb.Mappers
{
    public class PreviewViewModelMapper : IViewModelMapper<Preview, PreviewViewModel>
    {
        public PreviewViewModel MapFrom(Preview entity)
        {
            var previewViewModel = new PreviewViewModel
            {
                Id = entity.Id,
                Content = entity.Content,
                CreatedOn = entity.CreatedOn,
                Movie = entity.Movie
            };

            return previewViewModel;
        }
    }
}
