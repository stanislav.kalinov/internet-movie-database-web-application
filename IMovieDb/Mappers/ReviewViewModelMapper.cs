﻿using Core_2._2.Mappers.Contracts;
using IMovieDb.Data.Models;
using IMovieDb.Models;

namespace IMovieDb.Mappers.Contracts
{
    public class ReviewViewModelMapper : IViewModelMapper<Review, ReviewViewModel>
    {
        public ReviewViewModel MapFrom(Review entity)
        {
            var reviewViewModel = new ReviewViewModel
            {
                Id = entity.Id,
                Content = entity.Content,
                CreatedOn = entity.CreatedOn,
                Movie = entity.Movie

            };

            return reviewViewModel;
        }
    }
}
