﻿using Core_2._2.Mappers.Contracts;
using Core_2._2.Models;
using MovieDb.Data.Models;
using System.Linq;

namespace IMovieDb.Mappers
{
    public class ActorViewModelMapper : IViewModelMapper<Actor, ActorViewModel>
    {
        public ActorViewModel MapFrom(Actor entity)
        {
            var actorViewModel = new ActorViewModel
            {
                Id = entity.Id,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Birthday = entity.Birthday,
                Movies = entity.MoviesActors.Select(ma => ma.Movie).ToList()
            };

            return actorViewModel;
        }
    }
}
