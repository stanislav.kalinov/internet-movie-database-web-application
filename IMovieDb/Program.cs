﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using MovieDb.Data.Context;

namespace IMovieDb
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            MovieDbContext.SeedUserAdmin(host);

            host.Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
