﻿using Core_2._2.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace IMovieDb.Models
{
    public class ListMovieByGenreViewModel
    {
        public MoviesViewModel MoviesViewModel { get; set; }
        public IReadOnlyCollection<string> Genre { get; set; }

        private static SelectListGroup FeatureFilmGroup = new SelectListGroup { Name = "Feature film" };
        private static SelectListGroup DocumentaryGroup = new SelectListGroup { Name = "Documentary" };
        private static SelectListGroup AnimationGroup = new SelectListGroup { Name = "Animation" };

        public List<SelectListItem> GenresList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem()
            {
                Value = "Comedy",
                Text = "Comedy",
                Group = FeatureFilmGroup
            },
            new SelectListItem()
            {
                Value = "Drama",
                Text = "Drama",
                Group = FeatureFilmGroup
            },

            new SelectListItem()
            {
                Value = "Action",
                Text = "Action",
                Group = FeatureFilmGroup
            },
            new SelectListItem()
            {
                Value = "Thriller",
                Text = "Thriller",
                Group = FeatureFilmGroup
            },
            new SelectListItem()
            {
                Value = "Horror",
                Text = "Horror",
                Group = FeatureFilmGroup
            },
            new SelectListItem()
            {
                Value = "Western",
                Text = "Western",
                Group = FeatureFilmGroup
            },
            new SelectListItem()
            {
                Value = "Fantasy",
                Text = "Fantasy",
                Group = FeatureFilmGroup
            },
            new SelectListItem()
            {
                Value = "Crime",
                Text = "Crime",
                Group = FeatureFilmGroup
            },
            new SelectListItem
            {
                Value = "Historical",
                Text = "Historical",
                Group = DocumentaryGroup
            },
            new SelectListItem
            {
                Value = "Investigation",
                Text = "Investigation",
                Group = DocumentaryGroup
            },
            new SelectListItem
            {
                Value = "Medicine",
                Text = "Medicine",
                Group = DocumentaryGroup
            },
            new SelectListItem
            {
                Value = "Space",
                Text = "Space",
                Group = DocumentaryGroup
            },
            new SelectListItem
            {
                Value = "Technologies",
                Text = "Technologies",
                Group = DocumentaryGroup
            },
            new SelectListItem
            {
                Value = "Natural sciences",
                Text = "Natural sciences",
                Group = DocumentaryGroup
            },
            new SelectListItem
            {
                Value = "Science fiction",
                Text = "Science fiction",
                Group = DocumentaryGroup
            },
            new SelectListItem
            {
                Value = "Traditional animation",
                Text = "Traditional animation",
                Group = AnimationGroup
            },
            new SelectListItem
            {
                Value = "Stop motion",
                Text = "Stop motion",
                Group = AnimationGroup
            },
            new SelectListItem
            {
                Value = "Computer animation",
                Text = "Computer animation",
                Group = AnimationGroup
            }
        };
    }
}
