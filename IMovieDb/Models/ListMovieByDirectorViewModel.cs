﻿using Core_2._2.Models;
using System.ComponentModel.DataAnnotations;

namespace IMovieDb.Models
{
    public class ListMovieByDirectorViewModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(15)]
        [RegularExpression("[A-Za-z]+", ErrorMessage = "The First Name must contain only letters!")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(15)]
        [RegularExpression("[A-Za-z]+", ErrorMessage = "The Last Name must contain only letters!")]
        public string LastName { get; set; }
        public MoviesViewModel MoviesViewModel { get; set; }
    }
}

