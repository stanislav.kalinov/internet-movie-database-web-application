﻿using System.Collections.Generic;

namespace Core_2._2.Models
{
    public class MoviesViewModel
   {
        public IReadOnlyCollection<MovieViewModel> Movies { get; set; }
    }
}
