﻿using MovieDb.Data.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace Core_2._2.Models
{
    public class PreviewViewModel
    {
        [Required(ErrorMessage = "The Title must contain between 1 and 30 symbols!")]
        [StringLength(30, ErrorMessage = "The title must contain between 1 and 30 symbols!", MinimumLength = 1)]
        public string MovieTitle { get; set; }

        public int Id { get; set; }

        [Required]
        [MaxLength(300)]
        public string Content { get; set; }

        public DateTime CreatedOn { get; set; }

        public Movie Movie { get; set; }
    }
}
