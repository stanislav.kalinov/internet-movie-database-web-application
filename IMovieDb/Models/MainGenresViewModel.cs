﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core_2._2.Models
{
    public class MainGenresViewModel
    {
        [Required]
        public IReadOnlyCollection<string> MainGenres { get; set; }

        public List<SelectListItem> MainGenresList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem()
            {
                Value = "Feature film",
                Text = "Feature film",
            },
            new SelectListItem()
            {
                Value = "Documentary",
                Text = "Documentary",
            },

            new SelectListItem()
            {
                Value = "Animation",
                Text = "Animation",
            }
        };
    }
}
