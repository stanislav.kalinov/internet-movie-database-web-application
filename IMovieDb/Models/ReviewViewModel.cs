﻿using MovieDb.Data.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace IMovieDb.Models
{
    public class ReviewViewModel
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(300)]
        public string Content { get; set; }
        public int MovieId { get; set; }
        public DateTime CreatedOn { get; set; }
        public Movie Movie { get; set; }
    }
}



