﻿using MovieDb.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace IMovieDb.Models
{
    public class UserRatingViewModel
    {

        [Required(ErrorMessage = "Please enter an integer between 1 and 10!")]
        public int UserRating { get; set; }
        public int MovieId { get; set; }
        public Movie Movie { get; set; }
    }
}


