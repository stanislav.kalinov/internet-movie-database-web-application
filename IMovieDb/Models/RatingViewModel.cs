﻿using System.ComponentModel.DataAnnotations;

namespace Core_2._2.Models
{
    public class RatingViewModel
    {
        [Required(ErrorMessage = "The Title must contain between 1 and 30 symbols!")]
        [StringLength(30, ErrorMessage = "The title must contain between 1 and 30 symbols!", MinimumLength = 1)]
        public string MovieTitle { get; set; }

        [Required(ErrorMessage ="Please enter an integer between 1 and 10!")]
        public int Rating { get; set; }
    }
}
