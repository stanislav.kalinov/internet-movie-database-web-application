﻿using System.ComponentModel.DataAnnotations;

namespace Core_2._2.Models
{
    public class DeleteMovieViewModel
    {
        [Required(ErrorMessage = "The Title must contain between 1 and 30 symbols!")]
        [StringLength(30, ErrorMessage = "The title must contain between 1 and 30 symbols!", MinimumLength = 1)]
        public string MovieTitle { get; set; }
    }
}
