﻿using MovieDb.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core_2._2.Models
{
    public class ActorViewModel
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(15)]
        [RegularExpression("[A-Za-z]+", ErrorMessage = "The First Name must contain only letters!")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(15)]
        [RegularExpression("[A-Za-z]+", ErrorMessage = "The Last Name must contain only letters!")]
        public string LastName { get; set; }

        [DataType(DataType.Date)]
        public DateTime? Birthday { get; set; }

        public ICollection<Movie> Movies { get; set; }

        public MoviesViewModel MoviesViewModel { get; set; }
    }
 }
