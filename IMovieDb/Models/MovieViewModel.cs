﻿using IMovieDb.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using MovieDb.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core_2._2.Models
{
    public class MovieViewModel
    {
        public int Id { get; set; }

        [Required (ErrorMessage = "The Title must contain between 1 and 30 symbols!")]
        [StringLength(30, ErrorMessage = "The title must contain between 1 and 30 symbols!", MinimumLength = 1)]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please enter valid date as per sample provided!")]
        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }
        public string ImageURL { get; set; }

        [Required (ErrorMessage = "Please enter movie Duration in minutes!")]
        public int Duration { get; set; }

        public int? Rating { get; set; }
        public decimal? UserRating { get; set; }
        public int? Voters { get; set; }
        public Director Director { get; set; }
        
        public Preview Preview { get; set; }
        public Genre MainGenre { get; set; }
        public IList<Actor> Actors { get; set; }
        public ICollection<Review> Reviews { get; set; }
        
        public IReadOnlyCollection<Genre> MoviesGenres { get; set; }

        public IReadOnlyCollection<string> MovieGenres { get; set; }

        public static SelectListGroup FeatureFilmGroup = new SelectListGroup { Name = "Feature film" };
        public static SelectListGroup DocumentaryGroup = new SelectListGroup { Name = "Documentary" };
        public static SelectListGroup AnimationGroup = new SelectListGroup { Name = "Animation" };

        public List<SelectListItem> GenresList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem()
            {
                Value = "Comedy",
                Text = "Comedy",
                Group = FeatureFilmGroup
            },
            new SelectListItem()
            {
                Value = "Drama",
                Text = "Drama",
                Group = FeatureFilmGroup
            },

            new SelectListItem()
            {
                Value = "Action",
                Text = "Action",
                Group = FeatureFilmGroup
            },
            new SelectListItem()
            {
                Value = "Thriller",
                Text = "Thriller",
                Group = FeatureFilmGroup
            },
            new SelectListItem()
            {
                Value = "Horror",
                Text = "Horror",
                Group = FeatureFilmGroup
            },
            new SelectListItem()
            {
                Value = "Western",
                Text = "Western",
                Group = FeatureFilmGroup
            },
            new SelectListItem()
            {
                Value = "Fantasy",
                Text = "Fantasy",
                Group = FeatureFilmGroup
            },
            new SelectListItem()
            {
                Value = "Crime",
                Text = "Crime",
                Group = FeatureFilmGroup
            },
            new SelectListItem
            {
                Value = "Historical",
                Text = "Historical",
                Group = DocumentaryGroup
            },
            new SelectListItem
            {
                Value = "Investigation",
                Text = "Investigation",
                Group = DocumentaryGroup
            },
            new SelectListItem
            {
                Value = "Medicine",
                Text = "Medicine",
                Group = DocumentaryGroup
            },
            new SelectListItem
            {
                Value = "Space",
                Text = "Space",
                Group = DocumentaryGroup
            },
            new SelectListItem
            {
                Value = "Technologies",
                Text = "Technologies",
                Group = DocumentaryGroup
            },
            new SelectListItem
            {
                Value = "Natural sciences",
                Text = "Natural sciences",
                Group = DocumentaryGroup
            },
            new SelectListItem
            {
                Value = "Science fiction",
                Text = "Science fiction",
                Group = DocumentaryGroup
            },
            new SelectListItem
            {
                Value = "Traditional animation",
                Text = "Traditional animation",
                Group = AnimationGroup
            },
            new SelectListItem
            {
                Value = "Stop motion",
                Text = "Stop motion",
                Group = AnimationGroup
            },
            new SelectListItem
            {
                Value = "Computer animation",
                Text = "Computer animation",
                Group = AnimationGroup
            }
        };
    }
}
