﻿using MovieDb.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Core_2._2.Models
{
    public class ActorsViewModel
    {
        [Required(ErrorMessage = "The Title must contain between 1 and 30 symbols!")]
        [StringLength(30, ErrorMessage = "The title must contain between 1 and 30 symbols!", MinimumLength = 1)]
        public string MovieTitle { get; set; }

        [Required]
        [MaxLength(15)]
        [RegularExpression("[A-Za-z]+", ErrorMessage = "The First Name must contain only letters!")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(15)]
        [RegularExpression("[A-Za-z]+", ErrorMessage = "The Last Name must contain only letters!")]
        public string LastName { get; set; }

        [DataType(DataType.Date)]
        public DateTime? Birthday { get; set; }


        public IList<Actor> Actors { get; set; }
    }
}
