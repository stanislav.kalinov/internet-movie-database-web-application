﻿using System.Collections.Generic;

namespace Core_2._2.Models
{
    public class SearchViewModel
    {
        public IReadOnlyCollection<MovieViewModel> Movies { get; set; }
        public string MovieGenre { get; set; }
        public string MainGenre { get; set; }
        public string SearchString { get; set; }
    }
}
