﻿using Core_2._2.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace IMovieDb.Models
{
    public class ListMovieByMainGenreViewModel
    {
        [Required]
        public string MainGenre { get; set; }
        public MoviesViewModel MoviesViewModel { get; set; }
        public List<SelectListItem> MainGenresList { get; set; } = new List<SelectListItem>
        {
            new SelectListItem()
            {
                Value = "Feature film",
                Text = "Feature film",
            },
            new SelectListItem()
            {
                Value = "Documentary",
                Text = "Documentary",
            },

            new SelectListItem()
            {
                Value = "Animation",
                Text = "Animation",
            }
        };

    }
}
