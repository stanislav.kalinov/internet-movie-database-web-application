﻿using Core_2._2.Mappers.Contracts;
using Core_2._2.Models;
using IMovieDb.Data.Models;
using IMovieDb.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieDb.Data.Models;
using MovieDb.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace IMovieDb.Controllers
{
    public class MovieController : Controller
    {
        private readonly IMovieService movieService;
        private readonly IDirectorService directorService;
        private readonly IActorService actorService;
        private readonly IGenreService genreService;
        private readonly IPreviewService previewService;
        private readonly IViewModelMapper<Movie, MovieViewModel> movieMapper;
        private readonly IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel> moviesMapper;
        private readonly IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel> searchMapper;

        public MovieController(IMovieService movieService, IDirectorService directorService, IActorService actorService,  
            IViewModelMapper<IReadOnlyCollection<Movie>, MoviesViewModel> moviesMapper, IGenreService genreService,
            IPreviewService previewService, IViewModelMapper<Movie, MovieViewModel> movieMapper,
            IViewModelMapper<IReadOnlyCollection<Movie>, SearchViewModel> searchMapper)
        {
            this.movieMapper = movieMapper ?? throw new ArgumentNullException(nameof(movieMapper));
            this.movieService = movieService ?? throw new ArgumentNullException(nameof(movieService));
            this.directorService = directorService ?? throw new ArgumentNullException(nameof(directorService));
            this.actorService = actorService ?? throw new ArgumentNullException(nameof(actorService));
            this.genreService = genreService ?? throw new ArgumentNullException(nameof(genreService));
            this.previewService = previewService ?? throw new ArgumentNullException(nameof(previewService));
            this.moviesMapper = moviesMapper ?? throw new ArgumentNullException(nameof(moviesMapper));
            this.searchMapper = searchMapper ?? throw new ArgumentNullException(nameof(searchMapper));
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult AddMovie()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddMovie(MovieViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            var movie = await this.movieService.AddMovieAsync(model.Title, model.ReleaseDate, model.Duration);

            if (model.MovieGenres != null)
            {
                foreach (var item in model.MovieGenres)
                {
                    var genre = await this.genreService.AddGenreAsync(model.Title, item);
                }
            }

            if (model.Director?.FirstName != null && model.Director?.LastName != null)
            {
                var director = await this.directorService.AddDirectorAsync(model.Title, model.Director.FirstName, model.Director.LastName);
            }

            if (model.Rating != null)
            {
                movie = await this.movieService.AddRatingAsync(model.Title, (int) model.Rating);
            }

            if (model.Preview?.Content != null)
            {
                var preview = await this.previewService.AddPreviewAsync(model.Title, model.Preview.Content);
            }

            if (model.Actors != null)
            {
                foreach (var item in model.Actors)
                {
                    if (item.FirstName != null && item.LastName != null)
                    {
                        var actor = await this.actorService.AddActorAsync(model.Title, item.FirstName, item.LastName, item.Birthday);
                    }
                }
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult AddDirector()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddDirector(DirectorViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            var director = await this.directorService.AddDirectorAsync(model.MovieTitle, model.FirstName, model.LastName);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult AddActors()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddActors(ActorsViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            var actor = await this.actorService.AddActorAsync(model.MovieTitle, model.FirstName, model.LastName, model.Birthday);

            foreach (var item in model.Actors)
            {
                if (item.FirstName != null && item.LastName != null)
                {
                    actor = await this.actorService.AddActorAsync(model.MovieTitle, item.FirstName, item.LastName, item.Birthday);
                }
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult AddPreview()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddPreview(PreviewViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            var preview = await this.previewService.AddPreviewAsync(model.MovieTitle, model.Content);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult AddRating()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddRating(RatingViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            var movie = await this.movieService.AddRatingAsync(model.MovieTitle, model.Rating);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult AddGenres()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddGenres(GenresViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            foreach (var item in model.Genres)
            {
                var genre = await this.genreService.AddGenreAsync(model.MovieTitle, item);
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteGenres()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteGenres(GenresViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            foreach (var item in model.Genres)
            {
                await this.genreService.DeleteGenreAsync(model.MovieTitle, item);
            }

            return RedirectToAction("Index", "Home");
        }


        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult AddMainGenres()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddMainGenres(MainGenresViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            foreach (var item in model.MainGenres)
            {
                var genre = await this.genreService.AddMainGenreAsync(item);
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult ChangePreview()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePreview(PreviewViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            var preview = await this.previewService.ChangePreviewAsync(model.MovieTitle, model.Content);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult ChangeRating()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangeRating(RatingViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            var (movie, oldRating) = await this.movieService.ChangeRatingAsync(model.MovieTitle, model.Rating);

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteMovieModal(string MovieTitle)
        {
            await this.movieService.DeleteMovieAsync(MovieTitle);

            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteMovieModal(DeleteMovieViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            await this.movieService.DeleteMovieAsync(model.MovieTitle);

            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteMovie()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteMovieConfirmation(DeleteMovieViewModel model)
        {
            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteMovie(DeleteMovieViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            await this.movieService.DeleteMovieAsync(model.MovieTitle);

            return RedirectToAction("Index", "Home");
        }
        public async Task<IActionResult> Search(SearchViewModel model)
        {
            var movies = await this.movieService.FilterMoviesAsync(model.SearchString, model.MovieGenre, model.MainGenre);

            var searchViewModel = this.searchMapper.MapFrom(movies);

            return View(searchViewModel);
        }

        [HttpGet]
        [Authorize]
        // [ResponseCache(Location = ResponseCacheLocation.Client, Duration = 60)]
        public IActionResult ShowMoviesByActor()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> ShowMoviesByActor(ActorViewModel actorViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            var movies = await this.movieService.GetMoviesByActorAsync(actorViewModel.FirstName, actorViewModel.LastName);
            var moviesViewModel = this.moviesMapper.MapFrom(movies);
            actorViewModel.MoviesViewModel = moviesViewModel;
            return View(actorViewModel);
        }

        [HttpGet]
        [Authorize]
        public IActionResult ShowMoviesByDirector()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> ShowMoviesByDirector(ListMovieByDirectorViewModel listmovieViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            var movies = await this.movieService.GetMoviesByDirectorAsync(listmovieViewModel.FirstName, listmovieViewModel.LastName);
            var moviesViewModel = this.moviesMapper.MapFrom(movies);
            listmovieViewModel.MoviesViewModel = moviesViewModel;
            return View(listmovieViewModel);
        }

        [Authorize]
        public async Task<IActionResult> ShowTopRatedMoviesOnSite()
        {
            var movies = await this.movieService.GetTopRatedMovies();
            var moviesViewModel = this.moviesMapper.MapFrom(movies);

            return View(moviesViewModel);
        }
        public async Task<IActionResult> Details(int Id)
        {
            var movie = await this.movieService.GetMovieToShowAsync(Id);
            var movieViewModel = this.movieMapper.MapFrom(movie);

            return View(movieViewModel);
        }
        [HttpGet]
        [Authorize]
        public IActionResult ShowMoviesByReleaseYear()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> ShowMoviesByReleaseYear(MoviesViewModel moviesViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(moviesViewModel);
            }
            int releaseYear = Convert.ToInt32(HttpContext.Request.Form["txtFirst"].ToString());

            var movies = await this.movieService.GetMoviesByReleaseYearAsync(releaseYear);
            moviesViewModel = this.moviesMapper.MapFrom(movies);

            return View(moviesViewModel);
        }

        [HttpGet]
        [Authorize]
        public IActionResult ShowMoviesByReleaseDate()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> ShowMoviesByReleaseDate(MoviesViewModel moviesViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(moviesViewModel);
            }

            DateTime startYear = Convert.ToDateTime(Request.Form["txtFirst"]).Date;
            DateTime endYear = Convert.ToDateTime(Request.Form["txtSecond"]).Date;

            var movies = await this.movieService.GetMoviesByReleaseDateAsync(startYear, endYear);
            moviesViewModel = this.moviesMapper.MapFrom(movies);

            return View(moviesViewModel);
        }
        [HttpGet]
        [Authorize]
        public IActionResult ShowMoviesByMainGenre()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> ShowMoviesByMainGenre(ListMovieByMainGenreViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            
                var movies = await this.movieService.GetMoviesByMainGenreAsync(model.MainGenre);
                var moviesViewModel = this.moviesMapper.MapFrom(movies);
                model.MoviesViewModel = moviesViewModel;

            return View(model);
        }
        [HttpGet]
        [Authorize]
        public IActionResult ShowMoviesByGenre()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> ShowMoviesByGenre(ListMovieByGenreViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var movies = await this.movieService.GetMoviesByGenre(model.Genre);
            var moviesViewModel = this.moviesMapper.MapFrom(movies);
            model.MoviesViewModel = moviesViewModel;

            return View(model);
        }
        [HttpGet]
        [Authorize]
        public IActionResult AddReview(int id)
        {
            return View(new ReviewViewModel { MovieId = id });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddReview(ReviewViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

         await this.movieService.AddReviewAsync(model.MovieId, model.Content);

          return RedirectToAction("Details", new { id = model.MovieId});
        }
        [HttpGet]
        [Authorize]
        public IActionResult AddUserRating(int id)
        {
            return View(new UserRatingViewModel { MovieId = id });
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> AddUserRating(UserRatingViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
           await this.movieService.AddUserRatingAsync(model.MovieId, model.UserRating);

         return RedirectToAction("Details", new { id = model.MovieId });
        }

        [Authorize]
        public async Task<IActionResult> CommingSoon()
        {
            var movies = await this.movieService.GetCommingSoonMovies();
            var moviesViewModel = this.moviesMapper.MapFrom(movies);

            return View(moviesViewModel);
        }

        public async Task<IActionResult> ShowAllMovies(int? pageNumber)
        {
            var movies = this.movieService.AllMoviesQuery();

            int pageSize = 5;
            return View(await PaginatedList<Movie>.CreateAsync(movies, pageNumber ?? 1, pageSize));

        }
    }
}
